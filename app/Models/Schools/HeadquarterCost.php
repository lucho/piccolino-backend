<?php

namespace App\Models\Schools;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Schools\{Semester, Headquarter};

class HeadquarterCost extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'value',
        'headquarter_id',
        'observations',
        'purchase_concept',
        'resources_generated_by',
        'semester_id',
        'invoice_url',
        'date',
        'enabled'
    ];


    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }
    
    public function headquarter()
    {
        return $this->belongsTo(Headquarter::class);
    }
}

<?php

namespace App\Http\Middleware;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

use Exception;
use Closure;

/**
 * JwtMiddleware
 *
 * @author: Luis Garizabalo <lgarizabalo@serempre.com>
 * @date: 2021-12-01
 */
class JwtMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(["status"=> "ERROR", 'message' => "Token no es válido",'code' =>  "FP00001"], 423);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(["status"=> "ERROR", 'message' => "Token ha caducado",'code' =>  "FP00001"], 423);
            }else{
                return response()->json(["status"=> "ERROR", 'message' => "Token de autorización no encontrado",'code'=>  "FP00001"], 423);
            }
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Schools;

use App\Http\Controllers\Controller;
use App\Models\Schools\{HeadquarterCost};
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class HeadquarterCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'value' => 'required',
            'semester_id' => 'required|numeric',
            'headquarter_id' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return ($this->errorResponse($validator->errors(), 422));
        }
        try {
            $headquarterCost = HeadquarterCost::create($request->all());
            return ($this->showWithRelatedModels($headquarterCost, 200));
        } catch (Exception $e) {
            return ($this->errorResponse('No se logro llevar a cabo la operación.' . $e->getMessage(), 422));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schools\HeadquarterCost  $headquarterCost
     * @return \Illuminate\Http\Response
     */
    public function show(HeadquarterCost $headquarterCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schools\HeadquarterCost  $headquarterCost
     * @return \Illuminate\Http\Response
     */
    public function edit(HeadquarterCost $headquarterCost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schools\HeadquarterCost  $headquarterCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HeadquarterCost $headquarterCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schools\HeadquarterCost  $headquarterCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeadquarterCost $headquarterCost)
    {
        //
    }


    /**
     *
     */
    public function dataTable(Request $request)
    {
        $headquarterCosts = HeadquarterCost::with('semester')
            ->where('headquarter_id', $request['headquarter_id'])
            ->where(function ($query) use ($request) {
                $query->where('observations', 'like', '%' . $request->term . '%')
                    ->orWhere('date', 'like', '%' . $request->term . '%')
                    ->orWhere('purchase_concept', 'like', '%' . $request->term . '%')
                    ->orWhere('resources_generated_by', 'like', '%' . $request->term . '%')
                    ->orWhereHas('semester', function ($query) use ($request) {
                        return $query->where('code', 'like', '%' . $request->term . '%');
                    });
            })->paginate($request->limit)
            ->toArray();
        return $headquarterCosts;
    }

    /**
     *
     */
    public function dependences()
    {
        $controllers = [
            'Schools\SemesterController' => ['semesters', 'indexReport'],
            'Schools\HeadquarterController' => ['headquarters', 'index']
        ];
        $response = $this->jsonResource($controllers);
        return $response;
    }
}

<?php

namespace App\Http\Controllers\Schools;

use App\Http\Controllers\Controller;
use App\Models\Schools\Assistance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;
class AssistanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = $this->SpecialValidation($request, 'store');
        if ($valid['exist']) {
            return ($this->errorResponse('Ya registro la asistencia del grupo para este día en esta hora', 422));
        }

        $validator = Validator::make($request->all(), [
            'arrayStudents' => 'required',
            'cut_id' => 'required',
            'date' => 'required',
            'group_id' => 'required',
            'hour_id' => 'required',
            'semester_id' => 'required',
            'subject_id' => 'required'
        ]);
        if ($validator->fails()) {
            return ($this->errorResponse($validator->errors(), 422));
        }
        try {
            $headers = getallheaders();
            $assistance = new Assistance();
            DB::beginTransaction();

            if (count($request->input('arrayStudents'))) {
                foreach ($request->input('arrayStudents') as $arrayStudent) {
                $newAssistance =    Assistance::create([ 'group_id' =>$request['group_id'],
                    'subject_id'=>$request['subject_id'],
                    'enrolled_id'  => $arrayStudent['id'],
                    'people_id'=>$headers['people_id'],
                    'semester_id'=>$request['semester_id'],
                    'cut_id' => $request['cut_id'],
                    'hour_id' => $request['hour_id'],
                    'date' => $request['date'],
                    'assists'=> $arrayStudent['update']]);
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return ($this->errorResponse('Se presento un error en el sistema' . $e->getMessage(), 422));
        }
        return ($this->showWithRelatedModels($newAssistance, 200));
    }


     /**
     * Se encarga de validar que ya no este registrado la unidad de medida. 
     */
    public function SpecialValidation($request = null, $tipo = null)
    {
        try {
            $headers = getallheaders();
            $enrolled = Assistance::where('people_id', $headers['people_id'])->where('semester_id', $request['semester_id'])->where('subject_id', $request['subject_id'])->where('group_id', $request['group_id'])->where('hour_id', $request['hour_id'])->where('date', $request['date'])->first();
            if ($enrolled) {
                $validate['exist'] = true;
                return $validate;
            }
            $validate['exist'] = false;
            return $validate;
        } catch (Exception $e) {
            return ($this->errorResponse($e->getMessage(), 422));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schools\Assistance  $assistance
     * @return \Illuminate\Http\Response
     */
    public function show(Assistance $assistance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schools\Assistance  $assistance
     * @return \Illuminate\Http\Response
     */
    public function edit(Assistance $assistance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schools\Assistance  $assistance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assistance $assistance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schools\Assistance  $assistance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assistance $assistance)
    {
        //
    }

    /**
     * 
     */
    public function dependences()
    {
        $controllers = [
            'Schools\SemesterController' => ['semesters', 'index', 1],
            'Schools\DayController' => ['days', 'index'],
            'Schools\HourController' => ['hours', 'index'],
        ];
        $response = $this->jsonResource($controllers);
        return $response;
    }
}

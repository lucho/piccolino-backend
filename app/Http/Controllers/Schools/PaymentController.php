<?php

namespace App\Http\Controllers\Schools;

use App\Http\Controllers\Controller;
use App\Models\Schools\{Payment, Enrolled};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $payments = Payment::with('people', 'headquarter','enrolled.people','enrolled.semester')->where([['enabled', '1'], ['enrolled_id', $request['enrolled_id']]])->get();
            return $this->showAll($payments);
        } catch (Exception $e) {
            return ($this->errorResponse($e->getMessage(), 422));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'people_id' => 'required',
            'headquarter_id' => 'required',
            'value' => 'required',
            'enrolled_id' => 'required'
        ]);
        if ($validator->fails()) {
            return ($this->errorResponse($validator->errors(), 422));
        }
        try {
            /**
             * Buscamos la matricula
             */
            $enrolled = Enrolled::find($request['enrolled_id']);
            if (!$enrolled) {
                return ($this->errorResponse('No encontramos la matricula', 422));
            }
            /**
             * Buscamos los pagos realiados
             */
            $payments = Payment::where('enrolled_id', $enrolled->id)->get();
            $valueInit = 0;

            foreach ($payments as $key => $payment) {
                $valueInit = ($valueInit + $payment->value);
            }
            $valueInit = ($valueInit + $request['value']);
            if ($valueInit > $enrolled->cost) {
                return ($this->errorResponse('El aporte no puede ser mayor al costo de la matrícula', 422));
            }
            $payment =  Payment::create($request->all());
        } catch (Exception $e) {
            return ($this->errorResponse($e->getMessage() . 'Se presento un error en el sistema', 422));
        }
        return ($this->showWithRelatedModels($payment, 200));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Schools\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Schools\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Schools\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Schools\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewQuienesSomos()
    {
        // $events = Event::Where('enabled', 1)->get();
        // $pluginSegments = PluginSegment::Where('enabled', 1)->get();
        // $typeVisualModules = TypeVisualModule::Where('enabled', 1)->get();
       // return view('web.sightWho.sightWho.blade', ['events' => $events, 'typeVisualModules' => $typeVisualModules, 'event_id' => $event_id, 'pluginSegments' => $pluginSegments]);
       return view('web.sightWho.sightWho');
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewHistoria()
    {
       return view('web.history.history');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewNuestroFundador()
    {
       return view('web.ourFounder.ourFounder');
    }
    /**
     * Vistas de sedes
     */

     /**
     * La salle
     *
     * @return \Illuminate\Http\Response
     */
    public function viewLaSalle()
    {
       return view('web.theSalle.theSalle');
    }


     /**
     * Lisboa
     *
     * @return \Illuminate\Http\Response
     */
    public function viewLisboa()
    {
       return view('web.lisboa.lisboa');
    }
    /**
     * San Francisco
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSanFrancisco()
    {
       return view('web.sanFrancisco.sanFrancisco');
    }

    /**
     * Que hacemos
     */

    public function viewWeMake()
    {
       return view('web.weMake.weMake');
    }



    /**
     * Voluntariado
     */

    public function viewVolunteering()
    {
       return view('web.volunteering.volunteering');
    }

    /**
     * Recursos
     */

    public function viewResources()
    {
       return view('web.resources.resource');
    }

    /**
     * Convenios
     */

    public function viewConventions()
    {
       return view('web.conventions.convention');
    }

    /**
     * Nuestros símbolos
     */

    public function lounsSymboloe()
    {
       return view('web.lounsSymboloe.lounsSymboloe');
    }



    
    

}

@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
    <div class="blakish-overlay"></div>
    <div class="container">
        <div class="page-breadcrumb-content text-center">
            <div class="page-breadcrumb-title">
                <h2 class="breadcrumb-head black bold"><span>Trayectoria</span></h2>
            </div>
        </div>
    </div>
</section>
<!-- End of breadcrumb section
		============================================= -->


<section id="course-details" class="course-details-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="course-details-item">
                    <div class="course-single-pic mb30">
                        <img src="assets/img/queHacemos/1.jpg" alt="">
                    </div>
                    <div class="course-single-text">
                        <div class="course-title mt10 headline relative-position">
                            <h3><a href="#"> <b>Trayectoria</b></a> </h3>
                        </div>
                        <!-- <div class="course-details-content">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
									</p>
								</div> -->

                        <!-- <div class="course-details-category ul-li">
									<span>Course <b>Section:</b></span>
									<ul>
										<li><a href="#">SEction 1 </a></li>
										<li><a href="#">SEction 2 </a></li>
										<li><a href="#">SEction 3 </a></li>
										<li><a href="#">SEction 4  </a></li>
										<li><a href="#">SEction 5  </a></li>
									</ul>
								</div> -->
                    </div>
                </div>
                <!-- /course-details -->

                <div class="affiliate-market-guide mb65">
                    <div class="section-title-2 mb20 headline text-left">
                        <h2><span>Nuestro camino</span></h2>
                    </div>

                    <div class="affiliate-market-accordion">
                        <div id="accordion" class="panel-group">
                            <div class="panel">
                                <div class="panel-title" id="headingTwo">
                                    <div class="ac-head">

                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <span>1</span> PONENCIAS EN EVENTOS CIENTÍFICOS
                                        </button>
                                    </div>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. Ponencia 1 Simposio Internacional de Educación, Pedagogía e Investigación Educativa. ¿Cómo se configuran objetos de memorias a través de los lenguajes del arte contemporáneo? Camilo Igua Torres. Universidad Autónoma de Baja California - Mexicali, Red Iberoamericana de Pedagogía - REDIPE. Mexicali, México. 5, 6 y 7 de Noviembre de 2014.
                                        </p>
                                        <p>
                                            2. Ponencia II Encuentro Internacional de Arte y Educación “Memoria y Saber” ¿Cómo se configuran objetos de memorias a través de los lenguajes del arte contemporáneo? Camilo Igua Torres. Colegio Rafael Bernal Jiménez IED Sede B. Producciones El Mimo, Instituto Distrital de las Artes – IDARTES, 40X40 Currículo para la excelencia académica y la formación integral – SED. Bogotá, 30 de Mayo de 2014.
                                        </p>
                                        <p>
                                            3. Primer Puesto Proyecto con resultados de Investigación: Exposición “Memorias Plásticas”: ¿Cómo se configuran objetos de memorias en contextos marginales a través de los lenguajes del arte contemporáneo? Camilo Igua Torres. Evento Show and Tell. Convocatoria Dirección de Investigación e Innovación de la Universidad Sergio Arboleda. Bogotá, 14 y 15 de Noviembre de 2013.
                                        </p>
                                        <p>
                                            4. Ponencia VI Encuentro Nacional e internacional y VII regional de experiencias en educación popular y comunitaria (ASOINCA). Sistematización pedagógica de la escuela Piccolino: Una apuesta de transformación curricular. Juan Sebastián Castañeda. 7 al 11 de octubre de 2013.
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-title" id="headingOne">
                                    <div class="ac-head">

                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            <span>2</span> TESIS DE PREGRADO Y MAESTRÍA SOBRE LA EXPERIENCIA PEDAGÓGICA DE LA FUNDACIÓN PICCOLINO
                                        </button>
                                    </div>
                                </div>
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. Propuesta Cartilla de lecto – escritura para adultos de medios populares (2005) Diana Castelblanco Suárez. Licenciatura en Lenguas Modernas. Departamento de Lenguas Modernas. Facultad Ciencias de la Educación. Universidad de La Salle.
                                        </p>
                                        <p>
                                            2. Una propuesta de Educación Popular para adultos desde el estilo educativo Lasallista (2005) Claudia Villalobos Gómez. Departamento de Lenguas Modernas. Facultad Ciencias de la Educación. Universidad de La Salle.
                                        </p>
                                        <p>
                                            3. Aportes al Currículo en torno al tema del trabajo desde la perspectiva de la IAP y la Educación Popular: Escuela Piccolino – Lisboa (2007) Claudia Villalobos Gómez. Maestría en Educación. Universidad Externado de Colombia.
                                        </p>
                                        <p>
                                            4. Itinerancia Educativa, El saber de la Escuela Piccolino (2013) Marcelo Carvajal. Licenciatura en Educación Comunitaria con Énfasis en Derechos Humanos. Facultad de Educación. Universidad Pedagógica Nacional.
                                        </p>
                                        <p>
                                            5. Sistematización de la Experiencia Pedagógica de la Fundación Piccolino: Una apuesta de transformación Curricular desde la Escuela de Adultos No – Formal Piccolino (2014) Juan Sebastián Castañeda. Licenciatura en Educación Comunitaria con Énfasis en Derechos Humanos. Facultad de Educación. Universidad Pedagógica Nacional.
                                        </p>

                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-title" id="headingThree">
                                    <div class="ac-head">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <span>3</span> CONOCIMIENTO Y EXPERIENCIA EN LA ELABORACIÓN DE CARTILLAS O HERRAMIENTAS PEDAGÓGICAS
                                        </button>
                                        <!-- <div class="course-by">
                                            BY: <b>TONI KROSS</b>
                                        </div>
                                        <div class="leanth-course">
                                            <span>60 Minuttes</span>
                                            <span>Adobe photoshop</span>
                                        </div> -->
                                    </div>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. Cartilla (Trabajo de Grado): Propuesta Cartilla de lecto – escritura para adultos de medios populares (2005) Diana Castelblanco Suárez. Licenciatura en Lenguas Modernas. Departamento de Lenguas Modernas. Facultad Ciencias de la Educación. Universidad de La Salle.
                                        </p>
                                        <p>
                                            2. Cartilla Pedagógica Diplomado: LA PAZ ES DE TODOS. Guía didáctica como aporte para la comprensión de los diálogos de paz en La Habana entre el Gobierno Nacional y las FARC-EP (2014) Marcela Arriaga, Juan Sebastián Castañeda. Diplomado: Derecho a la verdad, Democracia y Agendas de Paz. Centro de Memoria, Paz y Reconciliación.
                                        </p>

                                    </div>
                                </div>
                            </div>



                            <div class="panel">
                                <div class="panel-title" id="headingFoure">
                                    <div class="ac-head">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false" aria-controls="collapseFoure">
                                            <span>4</span> RELATOS – NARRATIVIDADES DE MEMORIA
                                        </button>
                                    </div>
                                </div>
                                <div id="collapseFoure" class="collapse" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. “Caminar la educación: La experiencia de las Escuelas Piccolino”. Adriana Garzón, José Antonio Torres. En: Colombia. Vivir a Freire. Diálogos con Trabajo Social (2010) Catalina Echeverri González y María del Carmen Docal Millán (compiladoras) Corporación Universitaria Minuto de Dios.
                                        </p>
                                        <p>
                                            2. “Masacre”. Camilo Igua Torres, En: Colombia. Altus en Línea. Revista Virtual Universidad Sergio Arboleda. ISSN: 2216-005. Año 8 No. 2 Marzo de 2013.
                                        </p>
                                        <p>
                                            3. “Tinta sobre papel”. Luis Hernando Pineda, discapacitado físico habitante de una zona marginal de la localidad de Usaquén, en el marco del “Proyecto Historias de Vida, Fundación Piccolino”, desarrollado con él en el año 2012, En: Colombia. Altus en Línea. Revista Virtual Universidad Sergio Arboleda. ISSN: 2216-005. Año 8 No. 2 Marzo de 2013.
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-title" id="headingFoure">
                                    <div class="ac-head">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFoure1" aria-expanded="false" aria-controls="collapseFoure">
                                            <span>5</span> EXPERIENCIAS EN NARRATIVAS NO TEXTUALES
                                        </button>
                                    </div>
                                </div>
                                <div id="collapseFoure1" class="collapse" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. Exposición colectiva “Memorias Plásticas” (2013), técnica: Ensamblaje, Colegio Acacia II I.E.D., Fundación de Educación Informal Piccolino, Bogotá, Noviembre de 2013. </p>
                                        <p>
                                            2. Exposición colectiva Historias de Vida, “Tierra” (2012), técnica: Performance, Colegio Acacia II I.E.D., Fundación de Educación Informal Piccolino, Bogotá, Diciembre de 2012. </p>
                                        <p>
                                            3. Exposición colectiva Arte a la Lata, “Masacre” (2011), técnica: Ensamblaje, Plazoleta de las Mariposas, Universidad Sergio Arboleda, Bogotá, Octubre de 2012. </p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel">
                                <div class="panel-title" id="headingFoure">
                                    <div class="ac-head">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFoure2" aria-expanded="false" aria-controls="collapseFoure">
                                            <span>6</span> PROYECTOS DE INVESTIGACIÓN
                                        </button>
                                    </div>
                                </div>
                                <div id="collapseFoure2" class="collapse" data-parent="#accordion" style="">
                                    <div class="panel-body">
                                        <p>
                                            1. Febrero 2015 – Febrero 2016: Universidad Sergio Arboleda, Bogotá.

                                            Beca – pasantía Joven Investigador e Innovador (2015). Proyecto de investigación: Memorias Plásticas. Camilo Igua Torres. Adjudicación convocatoria interna 001 – 2014 Dirección de Investigación e Innovación vinculado a la Escuela de Filosofía y Humanidades y adscrito al grupo de Investigación reconocido por Colciencias “Estudios Históricos y Culturales.”
                                        </p>
                                        <p>
                                            2. Agosto 2013 – Septiembre 2015: Secretaría de Educación Distrital – SED –, Bogotá.

                                            Proyecto educativo “Alfabetización para Adultos” (Iniciativa 1609). INCITAR - Iniciativas Ciudadanas de Transformación de Realidades-, Participación para la Ciudadanía y la Convivencia.
                                        </p>
                                        <p>
                                            3. 2013 – 2014: Universidad Sergio Arboleda

                                            EQOS Inzá y Tierradentro (Semillero de investigación)

                                            Proyecto: “Precoperativa Inzá y Tierradentro” JEFE INVESTIGACIÓN: Prof. José Antonio Torres INVESTIGADORES (ESTUDIANTES): Lizeth Tatiana Ortiz León (Finanzas y Comercio Exterior), Adrián Camilo Cárdenas Molina (Administración de Empresas Públicas y Privadas) Jhon Freddy Hernández Ortiz (Contaduría Pública) Bogotá, EIAM - Universidad Sergio Arboleda.
                                        </p>
                                        <p>
                                            4. Julio 2013 – Noviembre 2013: Instituto Distrital de las Artes – IDARTES, Universidad Jorge Tadeo Lozano, Universidad Pedagógica Nacional y Fundación Arteria, Bogotá.

                                            Proyecto Memorias Plásticas. Camilo Igua Torres. Ganador Beca de Formación en Mediación del Arte. Pedagogía artística no formal en espacios de circulación del arte contemporáneo. Convocatoria de Artes Plásticas y Visuales, Programa Distrital de Estímulos 2013.
                                        </p>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
                <!-- /market guide -->

                <!-- /review overview -->

               
            </div>

            <div class="col-md-3">
                <div class="side-bar">
                    <div class="course-side-bar-widget">
                        <h3>Fundación <span>Piccolino</span></h3>
                        <div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
                        </div>
                       
                    </div>
                   

                </div>
            </div>
        </div>
    </div>
</section>




@endsection
<div class="log-in float-right">
    <a data-toggle="modal" data-target="#myModalLogin" href="#" style="color:#189239;">Iniciar Sesión</a>
    <!-- The Modal -->
    <div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header backgroud-style">
                    <div class="gradient-bg"></div>
                    <div class="popup-logo">
                      <img src="{{asset('assets/img/Logo/2.png')}}" alt=""> 
                    </div>
                    <div class="popup-text text-center">
                        <h2> 
                        <!-- <span>Login</span>  -->
                        </h2>
                        <p>
                        <!-- Login to our website, or <span>REGISTER</span> -->
                        </p>
                    </div>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <!-- <div class="facebook-login">
                        <a href="#">
                            <div class="log-in-icon">
                                <i class="fab fa-facebook-f"></i>
                            </div>
                            <div class="log-in-text text-center">
                                Login with Facebook
                            </div>
                        </a>
                    </div> -->
                    <div class="alt-text text-center"><a href="#">Login</a> </div>
                    <form method="POST" class="contact_form" id="loginform" action="{{ route('login') }}">
                    @csrf
                        <div class="contact-info">
                            <input class="name" name="email" type="email" placeholder="usuario">
                        </div>
                        <div class="contact-info">
                            <input class="pass" name="password" type="password" placeholder="Contraseña">
                        </div>
                        <div class="nws-button text-center white text-capitalize">
                            <button type="submit" value="Submit">Login</button>
                        </div>
                    </form>
                    <div class="log-in-footer text-center">
                        <p>Solo los administradores del sistema</p>
                        <!-- <p>** At least one telephone number is required.</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>About Page</title>

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/meanmenu.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/video.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/progess.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/Logo/3.png')}}">

	<link rel="stylesheet"  href="assets/css/colors/switch.css">
	<link href="{{asset('assets/css/colors/color-2.css')}}" rel="alternate stylesheet" type="text/css" title="color-2">
	<link href="{{asset('assets/css/colors/color-3.css')}}" rel="alternate stylesheet" type="text/css" title="color-3">
	<link href="{{asset('assets/css/colors/color-4.css')}}" rel="alternate stylesheet" type="text/css" title="color-4">
	<link href="{{asset('assets/css/colors/color-5.css')}}" rel="alternate stylesheet" type="text/css" title="color-5">
	<link href="{{asset('assets/css/colors/color-6.css')}}" rel="alternate stylesheet" type="text/css" title="color-6">
	<link href="{{asset('assets/css/colors/color-7.css')}}" rel="alternate stylesheet" type="text/css" title="color-7">
	<link href="{{asset('assets/css/colors/color-8.css')}}" rel="alternate stylesheet" type="text/css" title="color-8">
	<link href="{{asset('assets/css/colors/color-9.css')}}" rel="alternate stylesheet" type="text/css" title="color-9">

</head>

<body>

	<div id="preloader"></div>

	



	<!-- Start of Header section
		============================================= -->
        @include('web.header.header')

 	<!-- Start of Header section
 		============================================= --> 


	<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold"> <span>Nuestro Fundador</span></h2>
					</div>
					<!-- <div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">ABOUT</li>
						</ul>
					</div> -->
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->


	<!-- Start of about us content
		============================================= -->
		<section id="about-page" class="about-page-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="about-us-content-item">
							<div class="about-gallery">
								<div class="about-gallery-img grid-1">
									<img src="assets/img/fundador/jose.jpeg" alt="">
								</div>
								<div class="about-gallery-img grid-2">
                                <img src="assets/img/fundador/2.jpg" alt="">
								</div>
								<div class="about-gallery-img grid-2">
									<img src="assets/img/fundador/3.jpeg" alt="">
								</div>
								<div class="about-gallery-img grid-2">
									<img src="assets/img/fundador/6.png" alt="">
								</div>
							</div>
							<!-- /gallery -->

							<div class="about-text-item" style="text-align: justify; texto-justify: inter-palabra;">
								<div class="section-title-2  headline text-left">
									<h2> <span>José AntonioTorres Ramirez (QEPD)</span></h2>
								</div>
                                <p>
                                Nace el 15 de marzo de 1971, en Bogotá. Es el cuarto de seis hermanos. Estudió en el liceo Hermano Miguel La Salle el cual marcó toda su existencia pues de allí emergió el espíritu de servicio a las comunidades menos beneficiadas en búsqueda de la equidad social y con base en la espiritualidad en el Dios de la Vida.
                               </p>
								
								<p>
                                De formación profesional: Matemático y Licenciado en Ciencias de la Educación con especialización en Estudios Religiosos y Magister en educación, Coach Profesional y candidato a PH en Economía.
				               </p>
                               <p>
                               Durante su vida laboral estuvo vinculado en diferentes colegios como profesor de matemáticas y humanidades, coordinador y orientador. Fue docente universitario en las áreas de matemáticas en la Universidad San Buenaventura, CEDINPRO, Fundación Universitaria del Área Andina, Colegio Mayor de Cundinamarca y en La Universidad Sergio Arboleda donde logró participar además en cargos administrativos en las escuelas de administración y matemáticas y ser director de Bienestar Universitario.
                              </p>
                              <p>
                              Perteneció a diferentes congregaciones religiosas y de labor social: Grupo Juvenil Sendero, Juventudes Marianas Vicentinas, Hogar de la Medalla Milagrosa y Fundación Pocalana.
                              </p>
                              <p>
                              Desde su creación es director de la Fundación Piccolino siendo el mayor benefactor de la misma.
                              </p>
                              <p>
                              Se caracterizó por su espiritualidad inigualable, su humildad admirable, su alegría constante, su bondad sin limites, su mirada amorosa y comprensiva hacia los otros, su espíritu transformador de vidas, su amistad sincera. 
                              </p>

							  <p>Estuvo vinculado a la vida religiosa con el propósito de ordenarse como sacerdote, inicialmente en 1989 con los Hermanos de las Escuelas Cristianas y posteriormente con la comunidad de la Salle y el Seminario Mayor, decide no terminar este proceso. Sin embargo su mayor inspiración ocurre cuando al querer ser coherente con su proyecto de vida decide radicarse en un barrio popular donde habita población en condiciones de pobreza y vulnerabilidad, es así como compartiendo sus necesidades y en pro de un compromiso profundo de entrega a los más necesitados decide crear las escuelas Piccolino, con la idea de que la educación es la herramienta más óptima que se le puede brindar a los seres humanos.  </p>

							</div>
							<!-- /about-text -->

						</div>
					</div>

					<!-- <div class="col-md-3">
						<div class="side-bar-widget first-widget">
							<h2 class="widget-title text-capitalize"><span>tahu</span></h2>
							<div class="course-img">
								<img src="assets/img/fundador/4.png" alt="">
							</div>
							<div class="course-desc">
								<p> </p>
							</div>
							
						</div>

					</div> -->
				</div>
			</div>
		</section>
	<!-- End of about us content
		============================================= -->


	




	<!-- Start of footer section
		============================================= -->
        @include('web.footer.footer')

	<!-- End of footer section
		============================================= -->



		<!-- For Js Library -->
		<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
		<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/js/jarallax.js')}}"></script>
		<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/js/lightbox.js')}}"></script>
		<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
		<script src="{{asset('assets/js/scrollreveal.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
		<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
		<script src="{{asset('assets/js/gmap3.min.js')}}"></script>
		<script src="{{asset('assets/js/switch.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>

    	<script src="{{asset('assets/js/script.js')}}"></script>
	</body>
	</html>
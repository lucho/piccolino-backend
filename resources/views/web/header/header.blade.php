<!-- Start of Header section
		============================================= -->
<header>
	<div id="main-menu" class="main-menu-container">
		<div class="main-menu">
			<div class="container">
				<div class="navbar-default">
					<div class="navbar-header float-left">
						<a class="navbar-brand text-uppercase" href="#"><img src="{{asset('assets/img/Logo/logoP.png')}}" alt="logo"></a>
					</div><!-- /.navbar-header -->

					<!-- <div class="select-lang">
								<select>
									<option value="9" selected="">ENG</option>
									<option value="10">BAN</option>
									<option value="11">ARB</option>
									<option value="12">FRN</option>
								</select>
							</div> -->
					<!-- <div class="cart-search float-right ul-li">
								<ul>
									<li><a href="#"><i class="fas fa-shopping-bag"></i></a></li>
									<li>
										<button type="button" class="toggle-overlay search-btn">
											<i class="fas fa-search"></i>
										</button>
										<div class="search-body">
											<div class="search-form">
												<form action="#">
													<input class="search-input" type="search" placeholder="Search Here">
													<div class="outer-close toggle-overlay">
														<button type="button" class="search-close">
															<i class="fas fa-times"></i>
														</button>
													</div>
												</form>
											</div>
										</div>
									</li>
								</ul>
							</div> -->
					<!-- Login -->

					@include('web.login.login')
					<!-- /Login -->
					<!-- Collect the nav links, forms, and other content for toggling -->
					<nav class="navbar-menu float-right">
						<div class="nav-menu ul-li">
							<ul>
								<li><a href="/" style="color:#189239;">inicio</a></li>

								<li class="menu-item-has-children ul-li-block">
									<a href="#" style="color:#189239;">¿Quiénes somos?</a>
									<ul class="sub-menu">
										<li><a href="{{ route('sightWho') }}">Quiénes Somos</a></li>
										<li><a href="{{ route('history') }}">Historia</a></li>
										<li><a href="{{ route('ourFounder') }}">Nuestro Fundador</a></li>
										<li><a href="{{ route('lounsSymboloe') }}">Nuestros Símbolos</a></li>

									</ul>
								</li>
								<li class="menu-item-has-children ul-li-block">
									<a href="#" style="color:#189239;">Educación</a>
									<ul class="sub-menu">
										<li><a href="{{ route('theSalle') }}">La salle</a></li>
										<li><a href="{{ route('lisboa') }}">Lisboa</a></li>
										<li><a href="{{ route('sanFrancisco') }}">San Francisco</a></li>
									</ul>
								</li>

								<li><a href="{{ route('weMake') }}" style="color:#189239;">¿Qué hacemos?</a></li>
								<li class="menu-item-has-children ul-li-block">
									<a href="#" style="color:#189239;">Haz Parte</a>
									<ul class="sub-menu">
										<li><a href="{{ route('volunteering') }}">Voluntariado</a></li>
										<li><a href="{{ route('resources') }}">Recursos</a></li>
										<li><a href="{{ route('conventions') }}">Convenios</a></li>
									</ul>
								</li>

							</ul>
						</div>
					</nav>

					<div class="mobile-menu">
						<div class="logo"><a href="index-1.html"><img src="{{asset('assets/img/Logo/logoP.png')}}" alt="Logo"></a></div>
						<nav>
							<ul>
								<li><a href="/" style="color:#189239;">inicio</a></li>
								<li><a href="#">¿Quiénes somos?</a>
									<ul>
										<li><a href="{{ route('sightWho') }}">Quiénes Somos</a></li>
										<li><a href="{{ route('history') }}">Historia</a></li>
										<li><a href="{{ route('ourFounder') }}">Nuestro Fundador</a></li>
									</ul>
								</li>
								<li><a href="#">Educación</a>
									<ul>
										<li><a href="{{ route('theSalle') }}">La salle</a></li>
										<li><a href="{{ route('lisboa') }}">Lisboa</a></li>
										<li><a href="{{ route('sanFrancisco') }}">San Francisco</a></li>
									</ul>
								</li>

								<li><a href="{{ route('weMake') }}">¿Qué hacemos?</a></li>
								<li><a href="#">Haz Parte</a>
									<ul>
									<li><a href="{{ route('volunteering') }}">Voluntariado</a></li>
										<li><a href="{{ route('resources') }}">Recursos</a></li>
										<li><a href="{{ route('conventions') }}">Convenios</a></li>
									</ul>
								</li>

							</ul>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- Start of Header section
 		============================================= -->
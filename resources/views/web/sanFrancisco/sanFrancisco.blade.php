@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold"><span>San Francisco</span></h2>
					</div>
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->

<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								<img src="assets/img/Escuelas/sanfrancisco.png" alt="">
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><a href="#"><b>San Francisco </b></a> </h3>
								</div>
								<div class="course-details-content">
									<p>
                                    Nuestra sede de San Francisco es cedida por la casa de la cultura de ciudad bolívar, donde trabajamos la primaria.                                
                                </p>
									
								</div>
							</div>
						</div>


					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="course-side-bar-widget">
								<h3>Fundación <span>Piccolino</span></h3>
							
								<div class="like-course">
									<a href="#"><i class="fas fa-heart"></i></a>
								</div>
							</div>
							
							<div class="couse-feature ul-li-block">
								<ul>
									<li>Horario: Domingos de 8 a.m. a 1 p.m.</li>
									<li>Ubicación: Calle 64 C Sur No. 29-02. Colegio Distrital Rodrigo Lara Bonilla Sede B. Bogotá, D.C.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection

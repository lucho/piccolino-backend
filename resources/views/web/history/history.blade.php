@extends('web.app')

@section('content')




<!-- Start of breadcrumb section
		============================================= -->
<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
	<div class="blakish-overlay"></div>
	<div class="container">
		<div class="page-breadcrumb-content text-center">
			<div class="page-breadcrumb-title">
				<h2 class="breadcrumb-head black bold"> <span>HISTORIA</span></h2>
			</div>

		</div>
	</div>
</section>
<!-- End of breadcrumb section
		============================================= -->


<!-- Start of Checkout content
		============================================= -->
<section id="checkout" class="checkout-section">
	<div class="container">
		<div class="section-title mb45 headline text-center">
			<span class="subtitle text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
			<h2>Nuestra<span> Historia</span></h2>
		</div>

	</div>
</section>
<!-- End  of Checkout content
		============================================= -->

<section id="teacher-details" class="teacher-details-area">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="teacher-details-content mb45">
					<div class="row">
						<div class="col-md-6">
							<div class="teacher-details-img">
								<img src="assets/img/historias/1.png" alt="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="teacher-details-text">
								<div class="section-title-2  headline text-left">
									<h2>Veintiún cuadernos y veintiún lápices</h2>
									<div class="teacher-deg">
										Primeros voluntarios Santa Cecilia Alta
									</div>
								</div>
								<div class="teacher-desc-social ul-li" style="text-align: justify; texto-justify: inter-palabra;">
									<p>
										En el año 2002 en el barrio Arauquita ubicado en el nororiente de la ciudad de Bogotá contando con la asistencia de 21 niños/as desescolarizados del sector surge la primera escuela Piccolino, el proyecto comienza con el objetivo de abrir un espacio de atención educativa donde se asistía por voluntad propia. Los profesores emergen como personas de la comunidad que son capacitadas y quienes años después con el patrocinio de nuestro fundador logran acceder a estudios de educación superior en programas académicos relacionados con la pedagogía.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="side-bar">
					<div class="course-side-bar-widget">
						<h3>Fundación <span>Piccolino</span></h3>
						<div class="like-course">
							<a href="#"><i class="fas fa-heart"></i></a>
						</div>
					</div>
					<div class="couse-feature ul-li-block">
						<ul>
							<li>2003 - 2007: Santa Cecilia (Cerro-norte)</li>
							<li>2005: El recuerdo (Ciudad Bolivar)</li>
							<li>2005 : Lisboa (Suba)</li>
							<li>2006 - 2009 : Simijaca</li>
							<li>2007 : La Esperanza y San Francisco</li>
							<li>2008 - 2016 : La Salle</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="teacher-details-content mb45">
					<div class="row">
						<div class="col-md-6">
							<div class="teacher-details-img">
								<img src="assets/img/historias/2.png" alt="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="teacher-details-text">
								<div class="section-title-2  headline text-left">
									<h2>Niños Santa Cecilia Alta</h2>
									<!-- <div class="teacher-deg">
                                            Primeros voluntarios Santa Cecilia Alta 
											</div> -->
								</div>
								<div class="teacher-desc-social ul-li" style="text-align: justify; texto-justify: inter-palabra;">
									<p>
										El inicio de este camino fue matizado por múltiples retos: pocos recursos económicos, carencia de material didáctico, aulas no adecuadas para la enseñanza (iglesia del sector), dificultades en la adaptación al ambiente escolar, problemas de aprendizaje por parte de algunos niños/as y la existencia de un grupo parte de la población que nunca habían asistido a la escuela. Sin embargo el espíritu de servicio y altruismo de los maestros y de su fundador lograron enfrentar satisfactoriamente estos temas y así fortalecer una obra que encontraría en los matices de esta realidad su razón de ser.

										La escuela logra reconocimiento en la comunidad y al finalizar el primer año cuenta con 92 estudiantes, se adelantan entonces gestiones con entidades locales para obtener un cupo en las Instituciones de educación distrital
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="teacher-desc-social ul-li" style="text-align: justify; texto-justify: inter-palabra;">
						<p>
							y así lograr la reintegraciónde esta población a la escuela formal.
							Su carácter itinerante permitió que durante los años siguientes el proyecto se desplazará a diferentes sectores de la ciudad, dando así cumplimiento a su slogan: “donde quiera que haya alguien que quiera aprender y alguien que quiera enseñar, allí habrá una escuela Piccolino.”
							El proyecto de niños/as finaliza en el 2013 momento en el cual las políticas distritales se consolidan permitiendo el acceso y asequibilidad de manera más oportuna y eficaz a la población infantil y adolescente.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="teacher-details-content mb45">
					<div class="row">
						<div class="col-md-6">
							<div class="teacher-details-img">
								<img src="assets/img/historias/3.png" alt="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="teacher-details-text">
								<div class="section-title-2  headline text-left">
									<h2>Escuela de Adultos</h2>
									<div class="teacher-deg">
										Escuela Santa Cecilia
									</div>
								</div>
								<div class="teacher-desc-social ul-li" style="text-align: justify; texto-justify: inter-palabra;">
									<p>
									El proyecto con adultos surge en el 2003 por solicitud de padres de los niños de la escuela de niños del barrio Santa Cecilia y cuyo objetivo era brindar formación integral a adultos que no habían culminado sus estudios de primaria y/o bachillerato y generando a la vez un espacio de integración que les permitiera proyectar una visión esperanzadora de la vida. Las clases tenían lugar los días domingos en horas de la mañana para así permitir la asistencia y permanencia del alumnado. La escuela de adultos debía ofrecer continuidad y certificación a sus estudiantes por lo tanto adelanta un convenio con Coopevisa y posteriormente con Cafam (Caja de Compensación Familiar)
									</p>
								</div>
							</div>

						</div>
						<div class="teacher-desc-social ul-li" style="text-align: justify; texto-justify: inter-palabra;">
							<p>
							implementando una prueba diagnóstica para determinar el nivel de conocimiento y una posterior clasificación, para así generar un proceso de trabajo basado en cartillas, el cual fue modificado con los años. A diferencia de la escuela de niños los profesores eran voluntarios, los cuales inicialmente parten de un grupo de amigos del fundador, pero con el tiempo se logra consolidar un equipo de trabajo impulsado en la iniciativa de enseñar a quien quería aprender.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- End of course details section-->



@endsection
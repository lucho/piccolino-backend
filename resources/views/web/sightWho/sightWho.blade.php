

	<!-- Desde aqui la nueva plantilla -->

	<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fundación Piccolino</title>


	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/meanmenu.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/video.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/progess.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/Logo/3.png')}}">


	<link rel="stylesheet"  href="assets/css/colors/switch.css">
	<link href="{{asset('assets/css/colors/color-2.css')}}" rel="alternate stylesheet" type="text/css" title="color-2">
	<link href="{{asset('assets/css/colors/color-3.css')}}" rel="alternate stylesheet" type="text/css" title="color-3">
	<link href="{{asset('assets/css/colors/color-4.css')}}" rel="alternate stylesheet" type="text/css" title="color-4">
	<link href="{{asset('assets/css/colors/color-5.css')}}" rel="alternate stylesheet" type="text/css" title="color-5">
	<link href="{{asset('assets/css/colors/color-6.css')}}" rel="alternate stylesheet" type="text/css" title="color-6">
	<link href="{{asset('assets/css/colors/color-7.css')}}" rel="alternate stylesheet" type="text/css" title="color-7">
	<link href="{{asset('assets/css/colors/color-8.css')}}" rel="alternate stylesheet" type="text/css" title="color-8">
	<link href="{{asset('assets/css/colors/color-9.css')}}" rel="alternate stylesheet" type="text/css" title="color-9">

</head>

<body>

	<div id="preloader">

	</div>

	<!-- Start of Header section
		============================================= -->
		@include('web.header.header')

 	<!-- Start of Header section
 		============================================= -->


	<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold"><span>¿Quiénes somos?</span></h2>
					</div>
					<!-- <div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Details</li>
						</ul>
					</div> -->
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->


	<!-- Start of course details section
		============================================= -->
		<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								<img src="assets/img/piccolino/16.jpg" alt="">
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><a href="#"> <b>Quiénes Somos</b></a></h3>
								</div>
								<div class="course-details-content">
									<p>Somos una fundación que asume el compromiso de vivir la educación en un contexto de pares, donde estudiantes y docentes aprendemos mutuamente, por eso siempre nos sentimos entre maestros, apostando también, desde esta vivencia, a nuestro espíritu Piccolino.</p>
									<!-- <p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
									</p> -->
								</div>

								<!-- <div class="course-details-category ul-li">
									<span>Course <b>Section:</b></span>
									<ul>
										<li><a href="#">SEction 1 </a></li>
										<li><a href="#">SEction 2 </a></li>
										<li><a href="#">SEction 3 </a></li>
										<li><a href="#">SEction 4  </a></li>
										<li><a href="#">SEction 5  </a></li>
									</ul>
								</div> -->
							</div>
						</div>
						<!-- /course-details -->

						<div class="affiliate-market-guide mb65">
							<div class="section-title-2 mb20 headline text-left">
								<h2><span></span>Horizonte institucional</h2>
							</div>

							<div class="affiliate-market-accordion">
								<div id="accordion" class="panel-group">
									<div class="panel">
										<div class="panel-title" id="headingOne">
											<div class="ac-head">
												
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													<span>1
						                     </span>	Misión
						
												</button>
												<!-- <div class="course-by">
													BY: <b>TONI KROSS</b> 
												</div> -->
												<!-- <div class="leanth-course">
													<span>60 Minuttes</span>
													<span>Adobe photoshop</span>
												</div> -->
											</div>
										</div>
										<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="panel-body">
											Nuestra misión es ofrecer a jóvenes y adultos una propuesta educativa que tenga en cuenta sus necesidades y capacidades, aportando así a su calidad de vida y fomentando un cambio social.											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingTwo">
											<div class="ac-head">
												
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
													<span>2</span>	Visión
												</button>
												<!-- <div class="course-by">
													BY: <b>TONI KROSS</b> 
												</div> -->
												<!-- <div class="leanth-course">
													<span>60 Minuttes</span>
													<span>Adobe photoshop</span>
												</div> -->
											</div>
										</div>
										<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
											Piccolino en el 2025 es un referente de una comunidad de aprendizaje mutuo que resignifica armónicamente las trayectorias de vida de sus participantes.											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<!-- /market guide -->

						<div class="affiliate-market-guide mb65">
							<div class="section-title-2 mb20 headline text-left">
								<h2><span></span>Nuestros Principios</h2>
							</div>

							<div class="affiliate-market-accordion">
								<div id="accordion" class="panel-group">
									<div class="panel">
										<div class="panel-title" id="headingOne">
											<div class="ac-head">
												
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
													<span>1
						                     </span>	 EDUCACIÓN
						
												</button>
												
											</div>
										</div>
										<div id="collapseOne1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="panel-body">
											En Piccolino comprendemos la educación como un proceso permanente de dignificación que implica: aprendizaje mutuo, desarrollo de capacidades y prácticas innovadoras, para generar calidad de vida y transformación social.

                                            </div>
									</div>

									
								  <!-- panel -->
							</div>
							<div class="panel">
										<div class="panel-title" id="headingTwo">
											<div class="ac-head">
												
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="true" aria-controls="collapseTwo">
													<span>2</span>	 CORRESPONSABILIDAD
												</button>
											
											</div>
										</div>
										<div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
											Es el ejercicio político de pertenencia y participación, que nos compromete a hacer de la solidaridad, el respeto y el diálogo las bases de la transformación social que buscamos como ciudadanos del mundo.
										  </div>
									    </div>
								  </div>
								  <!-- /panel -->
								  <div class="panel">
										<div class="panel-title" id="headingTwo">
											<div class="ac-head">
												
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo">
													<span>3</span>	  ESPIRITUALIDAD
												</button>
											
											</div>
										</div>
										<div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
											Para Piccolino la espiritualidad es la vivencia plena de nuestros vínculos trascendentales, se nutre del amor y nos permite relacionarnos en medio de la confianza, la empatía y la comprensión.
                                            </div>
									    </div>
								  </div>
								  <!-- /panel -->
								  <div class="panel">
										<div class="panel-title" id="headingTwo">
											<div class="ac-head">
												
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="true" aria-controls="collapseTwo">
													<span>4</span>	   SERVICIO
												</button>
											
											</div>
										</div>
										<div id="collapseTwo3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
											El servicio en Piccolino es la actitud fundamental que nos lleva a comprometernos con el desarrollo de nuestras potencialidades y descubrir que en el bienestar del otro se encuentra nuestra propia felicidad.
                                          </div>
									    </div>
								  </div>
						</div>
						<!-- /market guide -->

					
						<!-- /review overview -->

						
					</div>

				
				</div>
			</div>
		</section>
	<!-- End of course details section
		============================================= -->	



	<!-- Start of footer section
		============================================= -->
			<section id="footer-area" class="footer-area-section">
				<div class="container">
					<div class="footer-content pb10">
						<div class="row">
							<div class="col-md-6">
								<div class="footer-widget">
									<div class="footer-logo mb35">
										<img src="assets/img/Logo/logoP.png" alt="">
									</div>
									<div class="section-title-2 mb20 headline text-left">
								<h2><span></span>Nuestros Estatutos</h2>
							</div>
									<div class="footer-about-text">
									<p style="text-align: justify;">La FUNDACIÓN PICCOLINO es una fundación de derecho privado sin ánimo de lucro, la cual es una persona jurídica que se regirá por las disposiciones de los siguientes estatutos y en lo que en estos no se hubiere previsto, por lo que disponga el Código Civil Colombiano y demás normas vigentes sobre la materia. Para ver el documento completo haga click <a title="Estatutos Fundacion Piccolino" href="{{asset('estatutos/estatutos2015.pdf')}}" target="_blank">AQUÍ</a>.</p>
									</div>
								</div>
							</div>
							
							<div class="col-md-6">
								<div class="footer-widget">
									<h2 class="widget-title">Galería</h2>
									<div class="photo-list ul-li">
										<ul>
											<li>
												<img src="assets/img/banner/pbanner_picolino_13.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_picolino_13.jpg" data-lightbox="roadtrip">
														<i class="fas fa-search"></i>
													</a>
												</div>
											</li>
											<li>
												<img src="assets/img/banner/pbanner_piccolino_64.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_piccolino_64.jpg" data-lightbox="roadtrip">
														<i class="fas fa-search"></i>
													</a>
												</div>
											</li>
											<li>
												<img src="assets/img/banner/pbanner_piccolino_20.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_piccolino_20.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
												</div>
											</li>
											<li>
												<img src="assets/img/banner/pbanner_picolino_33.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_picolino_33.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
												</div>
											</li>
											<li>
												<img src="assets/img/banner/pbanner_piccolino_22.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_piccolino_22.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
												</div>
											</li>
											<li>
												<img src="assets/img/banner/pbanner_piccolino_54.png" alt="">
												<div class="blakish-overlay"></div>
												<div class="pop-up-icon">
													<a href="assets/img/banner/banner_piccolino_54.jpg" data-lightbox="roadtrip">	<i class="fas fa-search"></i></a>
												</div>

											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div> 
					<!-- /footer-widget-content -->
				

					<div class="copy-right-menu">
						<div class="row">
							
							
						</div>
					</div>
				</div>
			</section>


			@include('web.footer.footer')

	<!-- End of footer section
		============================================= -->


		<script src="{{asset('assets/js/waypoints.min.js')}}"></script>

		<!-- For Js Library -->
		<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
		<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/js/jarallax.js')}}"></script>
		<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/js/lightbox.js')}}"></script>
		<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
		<script src="{{asset('assets/js/scrollreveal.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
		<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
		<script src="{{asset('assets/js/gmap3.min.js')}}"></script>
		<script src="{{asset('assets/js/switch.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>

    	<script src="{{asset('assets/js/script.js')}}"></script>
	</body>
	</html>


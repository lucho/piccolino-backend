<section id="contact_secound" class="contact_secound_section backgroud-style">
			<div class="container">
				<div class="contact_secound_content">
					<div class="row">
						<div class="col-md-6">
							<div class="contact-left-content">
								<div class="section-title  mb45 headline text-left">
									<span class="subtitle ml42  text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
									<h2><span>Contáctanos</span></h2>
									<!-- <p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet  ipsum dolor sit amet, consectetuer adipiscing elit.
									</p> -->
								</div>

								<div class="contact-address">
									<div class="contact-address-details">
										<div class="address-icon relative-position text-center float-left">
											<i class="fas fa-map-marker-alt"></i>
										</div>
										<div class="address-details ul-li-block">
											<ul>
												<li>Sede La Salle:  Cra. 5 No. 59A-44</li>
												<li>Sede Lisboa: Cl. 136 ##153 - 86</li>
											</ul>
										</div>
										
									</div>
									<div class="contact-address-details">
										<div class="address-icon relative-position text-center float-left">
											<i class="fas fa-map-marker-alt"></i>
										</div>
										<div class="address-details ul-li-block">
											<ul>
												<li>Sede San Francisco: Cl. 64 Sur #29-19.</li>
												<li>Sede Recuerdo Bajo: Salón comunal</li>
											</ul>
										</div>
										
									</div>

									<div class="contact-address-details">
										<div class="address-icon relative-position text-center float-left">
											<i class="fas fa-phone"></i>
										</div>
										<div class="address-details ul-li-block">
											<ul>
												<li><span>Principal: </span>320 818 8454</li>
												<li><span>Secundario: </span>322 911 4606 </li>
											</ul>
										</div>
									</div>

									<div class="contact-address-details">
										<div class="address-icon relative-position text-center float-left">
											<i class="fas fa-envelope"></i>
										</div>
										<div class="address-details ul-li-block">
											<ul>
												<li>representantelegal@fundacionpiccolino.com </li>
												<li>secretaria@fundacionpiccolino.com</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- <div class="col-md-6">
							<div class="contact_secound_form">
								<div class="section-title-2 mb65 headline text-left">
									<h2>Send Us a message</h2>
								</div>
								<form class="contact_form" action="#" method="POST" enctype="multipart/form-data">
									<div class="contact-info">
										<input class="name" name="name" type="text" placeholder="Your Name.">
									</div>
									<div class="contact-info">
										<input class="email" name="email" type="email" placeholder="Your Email">
									</div>
									<textarea  placeholder="Message."></textarea>
									<div class="nws-button text-center  gradient-bg text-capitalize">
										<button type="submit" value="Submit">SEND MESSAGE NOW <i class="fas fa-caret-right"></i></button> 
									</div>
								</form>
							</div>
						</div> -->
					</div>
				</div>
			</div>
			<div class="footer_2 backgroud-style">
				<div class="container">
					<div class="back-top text-center mb45">
						<a class="scrollup" href="#"><img src="{{asset('assets/img/banner/bt.png')}}" alt=""></a>
					</div>
					<div class="footer_2_logo text-center">
						<img src="assets/img/Logo/logoP.png" alt="">
					</div>

					<div class="footer_2_subs text-center">
						<p>“Si hay alguien que quiera aprender y alguien que desee enseñar, allí habrá una escuela Piccolino”</p>
						<!-- <div class="subs-form relative-position">
							<form action="#" method="post">
								<input class="course" name="course" type="email" placeholder="Email Address.">
								<div class="nws-button text-center  gradient-bg text-uppercase">
									<button type="submit" value="Submit">Subscribe now</button> 
								</div>
							</form>
						</div> -->
					</div>
					<div class="copy-right-menu">
						<div class="row">
							<div class="col-md-5">
								<div class="copy-right-text">
									<p>Copyright © 2021. All Rights Reserved By Lega Soft</p>
								</div>
							</div>
							<div class="col-md-3">
								<div class="footer-social  text-center ul-li">
									<ul>
										<li><a href="https://es-la.facebook.com/fundacion.piccolino/"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="https://twitter.com/fundacpiccolino"><i class="fab fa-twitter"></i></a></li>
										<!-- <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li> -->
									</ul>
								</div>
							</div>
							<!-- <div class="col-md-4">
								<div class="copy-right-menu-item float-right ul-li">
									<ul>
										<li><a href="#">License</a></li>
										<li><a href="#">Privacy & Policy</a></li>
										<li><a href="#">Term Of Service</a></li>
									</ul>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</section>
@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
    <div class="blakish-overlay"></div>
    <div class="container">
        <div class="page-breadcrumb-content text-center">
            <div class="page-breadcrumb-title">
                <h2 class="breadcrumb-head black bold"><span>Convenios</span></h2>
            </div>

        </div>
    </div>
</section>
<!-- End of breadcrumb section
		============================================= -->
<!-- Start of about us content
		============================================= -->
<section id="about-page" class="about-page-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="about-us-content-item">
                    <div class="about-gallery">
                        <div class="about-gallery-img grid-1">
                            <img src="assets/img/convenios/1.png" alt="">
                        </div>
                        <div class="about-gallery-img grid-2">
                            <img src="assets/img/convenios/2.png" alt="">
                        </div>
                        <div class="about-gallery-img grid-2">
                            <img src="assets/img/convenios/3.png" alt="">
                        </div>
                    </div>
                    <!-- /gallery -->

                    <div class="about-text-item">
                        <div class="section-title-2  headline text-left">
                            <h2><span>Convenios</span></h2>
                        </div>
                        <p>
                            Nos interesan los convenios de tipo académico en los que nuestros estudiantes puedan vincularse ya sea para estudios técnicos, tecnológicos o universitarios.
                        </p>
                        <p>
                            También son importantes los convenios laborales en los que nuestros estudiantes tengan acceso a trabajos que los dignifiquen como seres humanos, con sueldo y prestaciones de ley y buen trato.
                        </p>
                        <p>
                            Los convenios interinstitucionales con empresas que quieran aportar a nuestra fundación ya sea educación, empleo, dinero.
                        </p>
                        <p>
                            O empresas que deseen que nosotros como fundación les podamos ofertar nuestra experiencia en educación de adultos y/o en formación de voluntarios.
                        </p>
                    </div>
                    <!-- /about-text -->
                </div>
            </div>

            <div class="col-md-3">
                <div class="side-bar-widget first-widget">
                    <h2 class="widget-title text-capitalize"> Fundación <span>Piccolino </span></h2>
                    <div class="course-img">
                        <img src="assets/img/recursos/4.png" alt="">
                    </div>
                    <div class="course-desc">
                        <p>
                            “Si hay alguien que quiera aprender y alguien que desee enseñar, allí habrá una escuela Piccolino”
                        </p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!-- End of about us content
		============================================= -->



@endsection
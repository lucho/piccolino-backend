@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
    <div class="blakish-overlay"></div>
    <div class="container">
        <div class="page-breadcrumb-content text-center">
            <div class="page-breadcrumb-title">
                <h2 class="breadcrumb-head black bold"><span>Voluntariado</span></h2>
            </div>
            <!-- <div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Details</li>
						</ul>
					</div> -->
        </div>
    </div>
</section>
<!-- End of breadcrumb section
		============================================= -->

<section id="blog-detail" class="blog-details-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-details-content">
                    <div class="post-content-details">
                        <div class="blog-detail-thumbnile mb35">
                            <img src="assets/img/voluntariado/1.png" alt="">
                        </div>
                        <h2>Voluntariado</h2>

                        <!-- <div class="date-meta text-uppercase">
									<span><i class="fas fa-calendar-alt"></i> 26 April 2018</span>
									<span><i class="fas fa-user"></i> PRO.THEO HENRY</span>
									<span><i class="fas fa-comment-dots"></i> 15 COMMENTS</span>
								</div> -->
                        <h3>Voluntario. Bienvenido(a). </h3>
                        <p>
                            Nos da mucho gusto que quieras unirte a nosotros como voluntario(a) hemos establecido varias categorías para que puedas elegir. </p>

                    </div>
                    <div class="blog-share-tag">
                        <!-- <div class="share-text float-left">
									Share this news
								</div>
								<div class="share-social ul-li float-right">
									<ul>
										<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
										<li><a href="#"><i class="fab fa-twitter"></i></a></li>
										<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
									</ul>
								</div> -->
                    </div>
                    <!-- <div class="blog-category ul-li">
								<ul>
									<li><a href="#">fruits</a></li>
									<li><a href="#">veegetable</a></li>
									<li><a href="#">juices</a></li>
								</ul>
							</div> -->
                    <div class="author-comment">
                        <div class="author-img">
                            <img src="assets/img/voluntariado/2.png" alt="">
                        </div>
                        <div class="author-designation-comment">
                            <span>Voluntario Maestro</span>
                            <p>
                                Personas que quieran comprometerse con asistencia regular y liderar un proceso de formación con estudiantes, los domingos de 7 a 2 p.m., en cualquiera de nuestras sedes. </p>
                        </div>
                    </div>
                    <div class="next-prev-post">
                        <!-- <div class="next-post-item float-left">
									<a href="#"><i class="fas fa-arrow-circle-left"></i>Previous Post</a>
								</div>

								<div class="next-post-item float-right">
									<a href="#">Next Post<i class="fas fa-arrow-circle-right"></i></a>
								</div> -->
                    </div>
                    <div class="author-comment">
                        <div class="author-img">
                            <img src="assets/img/voluntariado/3.png" alt="">
                        </div>
                        <div class="author-designation-comment">
                            <span>Voluntario de apoyo para niños</span>
                            <p>
                                Personas que quieran donar su tiempo los domingos para el apoyo en el cuidado y la formación lúdica de los niños.
                            </p>
                        </div>
                    </div>
                    <div class="next-prev-post">

                    </div>

                    <div class="author-comment">
                        <div class="author-img">
                            <img src="assets/img/voluntariado/4.png" alt="">
                        </div>
                        <div class="author-designation-comment">
                            <span>Voluntarios profesionales con saberes específicos</span>
                            <p>
                                Personas que puedan vincularse y posean conocimientos relacionados con:
                            </p>
                            <p>
                                ....
                            </p>
                            <p>
                            1) Webmaster
                            </p>
                            <p>
                            2) Community manager
                            </p>
                            <p>
                            3) Contador
                            </p>
                            <p>
                            4) Publicista
                            </p>
                            <p>
                            5) Diseñador de proyectos
                            </p>
                            <p>
                            6) Gestor de redes sociales y humanas
                            </p>
                            <p>
                            7) Creativo
                            </p>
                            <p>
                            8) Coaches, psicólogos, terapeutas
                            </p>
                            <p>
                            9) Financieros
                            </p>
                            <p>
                            10) Asesor jurídico en familia
                            </p>
                            <p>
                            11) Maestros que se interesen por la educación popular
                            </p>
                            <p>
                            12) Mercaderista
                            </p>
                            <p>
                            13) Ventas
                            </p>
                            <p>
                            14) Administrador de empresas, economista (emprendimiento)
                            </p>
                            <p>
                            15) Comunicador social, periodista
                            </p>
                            <p>
                            16) Investigador antropológico
                            </p>
                            <p>
                            17) Profesionales en el área de la salud
                            </p>
                        </div>
                    </div>
                    <div class="next-prev-post">

                    </div>

                    <div class="author-comment">
                        <div class="author-img">
                            <img src="assets/img/voluntariado/5.png" alt="">
                        </div>
                        <div class="author-designation-comment">
                            <span>Voluntario Amigo</span>
                            <p>
                            Personas que quieren donar dinero, conseguir recursos, buscar alimentos para eventos especiales de la Fundación o que esporádicamente quiera colaborar con la Fundación.                            </p>
                        </div>
                    </div>
                    <div class="next-prev-post">

                    </div>
                </div>

            </div>

            <div class="col-md-3">
                <div class="side-bar">
                    <!-- <div class="side-bar-search">
                        <form action="#" method="get">
                            <input type="text" class="" placeholder="Search">
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div> -->

                    <div class="side-bar-widget">
                        <h2 class="widget-title text-capitalize">Fundación <span>Piccolino</span></h2>
                        <div class="post-categori ul-li-block">
                            <!-- <ul>
                                <li class="cat-item"><a href="#">Design Graphic Book</a></li>
                                <li class="cat-item"><a href="#">Student Bag’s</a></li>
                                <li class="cat-item"><a href="#">Education T-shirt</a></li>
                                <li class="cat-item"><a href="#">Student Watch</a></li>
                                <li class="cat-item"><a href="#">Tutorial Videos</a></li>
                                <li class="cat-item"><a href="#">Other Products</a></li>
                            </ul> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


@endsection
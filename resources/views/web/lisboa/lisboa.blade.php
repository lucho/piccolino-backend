@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold"><span>Lisboa</span></h2>
					</div>
					<!-- <div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Details</li>
						</ul>
					</div> -->
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->

<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								<img src="assets/img/Escuelas/lisboa.png" alt="">
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><a href="#"><b>Lisboa </b></a> </h3>
								</div>
								<div class="course-details-content">
									<p>
                                    Sede cedida gentilmente por la iglesia del Beato Juan Bautista Scalabrini desde el año 2005. En la actualidad solo se enseña primaria.                                          </p>
									
								</div>

								<!-- <div class="course-details-category ul-li">
									<span>Course <b>Section:</b></span>
									<ul>
										<li><a href="#">SEction 1 </a></li>
										<li><a href="#">SEction 2 </a></li>
										<li><a href="#">SEction 3 </a></li>
										<li><a href="#">SEction 4  </a></li>
										<li><a href="#">SEction 5  </a></li>
									</ul>
								</div> -->
							</div>
						</div>
						<!-- /course-details -->

						<!-- /market guide -->

						
						<!-- /review overview -->

					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="course-side-bar-widget">
								<h3>Fundación <span>Piccolino</span></h3>
							
								<div class="like-course">
									<a href="#"><i class="fas fa-heart"></i></a>
								</div>
							</div>
							
							<div class="couse-feature ul-li-block">
								<ul>
									<li>Horario: Domingos de 8 a.m. a 1 p.m.</li>
									<li>Ubicación: Iglesia Beato Juan Bautista Scalabrini. Diag. 133A No. 153-86, Barrio Lisboa (Suba), Bogotá, D.C.</li>
									<!-- <li>Video  <span>8 Hours</span></li>
									<li>Duration <span>30 Days</span></li>
									<li>Includes  <span>Breakfast</span></li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection

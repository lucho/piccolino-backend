@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
		<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
			<div class="blakish-overlay"></div>
			<div class="container">
				<div class="page-breadcrumb-content text-center">
					<div class="page-breadcrumb-title">
						<h2 class="breadcrumb-head black bold"><span>La Salle</span></h2>
					</div>
					<!-- <div class="page-breadcrumb-item ul-li">
						<ul class="breadcrumb text-uppercase black">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active">Details</li>
						</ul>
					</div> -->
				</div>
			</div>
		</section>
	<!-- End of breadcrumb section
		============================================= -->

<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								<img src="assets/img/Escuelas/lasalle.png" alt="">
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><a href="#"><b>LA SALLE </b></a> </h3>
								</div>
								<div class="course-details-content">
									<p>
                                    La sede de La Salle chapinero es cedida gentilmente por los hermanos de La Salle desde el año 2008. Aquí nos enfocamos principalmente en el desarrollo del bachillerato.                          
                                          </p>
									
								</div>

								<!-- <div class="course-details-category ul-li">
									<span>Course <b>Section:</b></span>
									<ul>
										<li><a href="#">SEction 1 </a></li>
										<li><a href="#">SEction 2 </a></li>
										<li><a href="#">SEction 3 </a></li>
										<li><a href="#">SEction 4  </a></li>
										<li><a href="#">SEction 5  </a></li>
									</ul>
								</div> -->
							</div>
						</div>
						<!-- /course-details -->

						<!-- /market guide -->

						
						<!-- /review overview -->

					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="course-side-bar-widget">
								<h3>Fundación <span>Piccolino</span></h3>
							
								<div class="like-course">
									<a href="#"><i class="fas fa-heart"></i></a>
								</div>
							</div>
							
							<div class="couse-feature ul-li-block">
								<ul>
									<li>Horario:  Domingos de 7 a.m. a 2 p.m</li>
									<li>Ubicación:  Cra. 5 No. 59A-44</li>
									<!-- <li>Video  <span>8 Hours</span></li>
									<li>Duration <span>30 Days</span></li>
									<li>Includes  <span>Breakfast</span></li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection

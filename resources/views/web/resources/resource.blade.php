@extends('web.app')

@section('content')

<!-- Start of breadcrumb section
		============================================= -->
<section id="breadcrumb" class="breadcrumb-section relative-position backgroud-style">
    <div class="blakish-overlay"></div>
    <div class="container">
        <div class="page-breadcrumb-content text-center">
            <div class="page-breadcrumb-title">
                <h2 class="breadcrumb-head black bold"><span>Recursos</span></h2>
            </div>

        </div>
    </div>
</section>
<!-- End of breadcrumb section
		============================================= -->
<!-- Start of about us content
		============================================= -->
<section id="about-page" class="about-page-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="about-us-content-item">
                    <div class="about-gallery">
                        <div class="about-gallery-img grid-1">
                            <img src="assets/img/recursos/1.png" alt="">
                        </div>
                        <div class="about-gallery-img grid-2">
                            <img src="assets/img/recursos/2.png" alt="">
                        </div>
                        <div class="about-gallery-img grid-2">
                            <img src="assets/img/recursos/3.png" alt="">
                        </div>
                    </div>
                    <!-- /gallery -->

                    <div class="about-text-item">
                        <div class="section-title-2  headline text-left">
                            <h2><span>Recursos</span></h2>
                        </div>
                        <p>
                            Personas que quieren donar dinero, conseguir recursos, buscar alimentos para eventos especiales de la Fundación o que esporádicamente quiera colaborar con la Fundación.
                        </p>
                        <p>
                            En esta categoría recibimos:
                        </p>
                        <p>
                            1) Artículos de papelería cuadernos, resmas de papel, lápices, esferos, colores, papel kraft, cartulina
                        </p>
                        <p>
                            2) Muebles, ropa, zapatos que se encuentren en buen estado y que nos sirvan para ofrecerlos a precios bajos a nuestros estudiantes en los bazares que realizamos con ese fin.
                        </p>
                        <p>
                            Si tienes algo más que quieras donar y no aparece en este listado, contáctanos y hablamos. Información de contacto: contacto@fundacionpiccolino.org
                        </p>
                        <p>
                            Dinero recibimos donaciones de $10.000.oo en adelante: Cuenta Davivienda de Ahorros No. 009400367992, no enviamos personas a recibir dinero.
                        </p>

                    </div>
                    <!-- /about-text -->


                </div>
            </div>

            <div class="col-md-3">
                <div class="side-bar-widget first-widget">
                    <h2 class="widget-title text-capitalize"> Fundación <span>Piccolino </span></h2>
                    <div class="course-img">
                        <img src="assets/img/recursos/4.png" alt="">
                    </div>
                    <div class="course-desc">
                        <p>
                            “Si hay alguien que quiera aprender y alguien que desee enseñar, allí habrá una escuela Piccolino”
                        </p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>
<!-- End of about us content
		============================================= -->



@endsection
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Fundación Piccolino</title>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/Logo/3.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/meanmenu.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/video.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/progess.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

	<link rel="stylesheet"  href="assets/css/colors/switch.css">
	<link href="{{asset('assets/css/colors/color-2.css')}}" rel="alternate stylesheet" type="text/css" title="color-2">
	<link href="{{asset('assets/css/colors/color-3.css')}}" rel="alternate stylesheet" type="text/css" title="color-3">
	<link href="{{asset('assets/css/colors/color-4.css')}}" rel="alternate stylesheet" type="text/css" title="color-4">
	<link href="{{asset('assets/css/colors/color-5.css')}}" rel="alternate stylesheet" type="text/css" title="color-5">
	<link href="{{asset('assets/css/colors/color-6.css')}}" rel="alternate stylesheet" type="text/css" title="color-6">
	<link href="{{asset('assets/css/colors/color-7.css')}}" rel="alternate stylesheet" type="text/css" title="color-7">
	<link href="{{asset('assets/css/colors/color-8.css')}}" rel="alternate stylesheet" type="text/css" title="color-8">
	<link href="{{asset('assets/css/colors/color-9.css')}}" rel="alternate stylesheet" type="text/css" title="color-9">

</head>

<body>

	<div id="preloader"></div>

<!-- 
	<div id="switch-color" class="color-switcher">
		<div class="open"><i class="fas fa-cog fa-spin"></i></div>
		<h4>COLOR OPTION</h4>
		<ul>
			<li><a class="color-2" onclick="setActiveStyleSheet('color-2'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-3" onclick="setActiveStyleSheet('color-3'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-4" onclick="setActiveStyleSheet('color-4'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-5" onclick="setActiveStyleSheet('color-5'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-6" onclick="setActiveStyleSheet('color-6'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-7" onclick="setActiveStyleSheet('color-7'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-8" onclick="setActiveStyleSheet('color-8'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
			<li><a class="color-9" onclick="setActiveStyleSheet('color-9'); return true;" href="#!"><i class="fas fa-circle"></i></a> </li>
		</ul>
		<button class="switcher-light">WIDE </button>
		<button class="switcher-dark">BOX </button>
		<a class="rtl-v" href="RTL_Genius/index.html">RTL </a>
	</div>
 -->



	<!-- Start of Header section
		============================================= -->
		@include('web.header.header')

 	<!-- Start of Header section
 		============================================= -->



 	<!-- Start of slider section
 		============================================= -->
 		<section id="slide" class="slider-section">
 			<div id="slider-item" class="slider-item-details">
 				<div  class="slider-area slider-bg-5 relative-position">
 					<div class="slider-text">
 						<div class="section-title mb20 headline text-left ">
 							<div class="layer-1-1">
 								<span class="subtitle ml42 text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
 							</div>
 							<div class="layer-1-3">
 								<h2><span>Estamos</span> entre <br>  <span>Maestros</span></h2>
 							</div>
 						</div>
 						<!-- <div class="layer-1-4">
 							<div class="genius-btn  text-center text-uppercase ul-li-block bold-font">
 								<a href="#">Our Courses <i class="fas fa-caret-right"></i></a>
 							</div>
 						</div> -->
 					</div>

 				</div>
 				<div class="slider-area slider-bg-2 relative-position">
 					<div class="slider-text">
 						<div class="section-title mb20 headline text-center ">
 							<div class="layer-1-1">
 								<span class="subtitle text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
 							</div>
 							<div class="layer-1-2">
 								<h2 class="secoud-title">  <span>“Si hay alguien que quiera aprender y alguien que desee enseñar, allí habrá una escuela Piccolino”</span></h2>
 							</div>
 						</div>
 						<!-- <div class="layer-1-3">
 							<div class="search-course mb30 relative-position">
 								<form action="#" method="post">
 									<input class="course" name="course" type="text" placeholder="Type what do you want to learn today?">
 									<div class="nws-button text-center  gradient-bg text-capitalize">
 										<button type="submit" value="Submit">Search Course</button> 
 									</div>
 								</form>
 							</div>
 							<div class="layer-1-4">
 								<div class="slider-course-category ul-li text-center">
 									<span class="float-left">BY CATEGORY:</span>
 									<ul>
 										<li>Graphics Design</li>
 										<li>Web Design</li>
 										<li>Mobile Application</li>
 										<li>Enginering</li>
 										<li>Science</li>
 									</ul>
 								</div>
 							</div>
 						</div> -->
 					</div>
 				</div>
 				<div class="slider-area slider-bg-3 relative-position">
 					<div class="slider-text">
 						<div class="layer-1-2">
 							<!-- <div class="coming-countdown ul-li">
 								<ul>
 									<li class="days">
 										<span class="number"></span>
 										<span class>Days</span>
 									</li>

 									<li class="hours">
 										<span class="number"></span>
 										<span class>Hours</span>
 									</li>

 									<li class="minutes">
 										<span class="number"></span>
 										<span class>Minutes</span>
 									</li>

 									<li class="seconds">
 										<span class="number"></span>
 										<span class>Seconds</span>
 									</li>
 								</ul>
 							</div> -->
 						</div>
 						<div class="section-title mb20 headline text-center ">
 							<div class="layer-1-3">
 								<h2 class="third-slide" style="color:#4cb7f5;"> Atrévete a soñar con <br> <span> nosotros</span></h2>
 							</div>
 						</div>
 						<!-- <div class="layer-1-4">
 							<div class="about-btn text-center">
 								<div class="genius-btn text-center text-uppercase ul-li-block bold-font">
 									<a href="#">About Us <i class="fas fa-caret-right"></i></a>
 								</div>
 							</div>
 						</div> -->
 					</div>
 				</div>
 				<div class="slider-area slider-bg-4 relative-position">
 					<div class="slider-text">
 						<div class="section-title mb20 headline text-center ">
 							<span class="subtitle text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
 							<h2 class=""  style="color:#4cb7f5;" ><span>Crece con nosotros</span> </h2>
 						</div>
 					</div>
 				</div>
 			</div>
 		</section>
	<!-- End of slider section ============================================= -->
	<!-- Start of course details section
		============================================= -->
		<section id="course-details" class="course-details-section">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="course-details-item">
							<div class="course-single-pic mb30">
								<img src="assets/img/banner/banner_piccolino_28.jpg" alt="">
							</div>
							<div class="course-single-text">
								<div class="course-title mt10 headline relative-position">
									<h3><a href="#"> <b>Piccolino</b></a> </h3>
								</div>
								<div class="course-details-content">
									<p> Piccolino es una apuesta de transformación desde la educación para adultos. 
										Es una experiencia de vida que lleva 20 años de creación y 11 años gestionando
										como fundación procesos de transformación desde la educación no formal. </p>

										<p> 
                                        Si quieres saber más para potenciar tu servicio y la vida de otras personas. </p>
									
								</div>

								<div class="course-details-category ul-li">
									<ul>
										<li><a href="#">contáctanos!</a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- /course-details -->

						
						<!-- /market guide -->

						
						<!-- /review overview -->

					
					</div>

					<div class="col-md-3">
						<div class="side-bar">
							<div class="course-side-bar-widget">
								<h3>Fundación <span>Piccolino</span></h3>
								<!-- <div class="genius-btn gradient-bg text-center text-uppercase float-left bold-font">
									<a href="#">Enroll THis course <i class="fas fa-caret-right"></i></a>
								</div> -->
								<div class="like-course">
									<a href="#"><i class="fas fa-heart"></i></a>
								</div>
							</div>
							<!-- <div class="enrolled-student">
								<div class="comment-ratting float-left ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div>
								<div class="student-number bold-font">
									250 Enrolled
								</div>
							</div> -->
							<div class="couse-feature ul-li-block">
								<ul>
									<li>Maestros Estudaintes <span>98</span></li>
									<li>Egresados  <span>247</span></li>
									<li>Voluntarios  <span>24</span></li>
									<li>Cofundadores <span>5</span></li>
									<li>Fundador  <span>1</span></li>
								</ul>
							</div>

							<div class="side-bar-widget">
								<h2 class="widget-title text-capitalize">Últimos <span>Eventos</span></h2>
								<div class="latest-news-posts">
									<div class="latest-news-area">
										<div class="latest-news-thumbnile relative-position">
											<img src="assets/img/piccolino/12.jpg" alt="">
											<div class="hover-search">
												<i class="fas fa-search"></i>
											</div>
											<div class="blakish-overlay"></div>
										</div>
										<div class="date-meta">
											<i class="fas fa-calendar-alt"></i> 06/09/2021
										</div>
										<h3 class="latest-title bold-font"><a href="#">Encuentro de escuelas</a></h3>
									</div>
									<!-- /post -->

									<div class="latest-news-posts">
										<div class="latest-news-area">
											<div class="latest-news-thumbnile relative-position">
												<img src="assets/img/piccolino/19.jpeg" alt="">
												<div class="hover-search">
													<i class="fas fa-search"></i>
												</div>
												<div class="blakish-overlay"></div>
											</div>
											<div class="date-meta">
												<i class="fas fa-calendar-alt"></i> 11/08/2021
											</div>
											<h3 class="latest-title bold-font"><a href="#">Encuentro de voluntarios</a></h3>
										</div>
										<!-- /post -->
									</div>

									<!-- <div class="view-all-btn bold-font">
										<a href="#">View All News <i class="fas fa-chevron-circle-right"></i></a>
									</div> -->
								</div>
							</div>

							<div class="side-bar-widget">
								<h2 class="widget-title text-capitalize"><span>próximo</span> evento.</h2>
								<div class="featured-course">
									<div class="best-course-pic-text relative-position">
										<div class="best-course-pic relative-position">
											<img src="assets/img/piccolino/18.jpg" alt="">
											<div class="trend-badge-2 text-center text-uppercase">
												<i class="fas fa-bolt"></i>
												<span>06/11/2021</span>
											</div>
										</div>
										<div class="best-course-text">
											<!-- <div class="course-title mb20 headline relative-position">
												<h3><a href="#">Capacitación de &amp; Voluntarios.</a></h3>
											</div> -->
											<!-- <div class="course-meta">
												<span class="course-category"><a href="#">Web Design</a></span>
												<span class="course-author"><a href="#">250 Students</a></span>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- End of course details section
		============================================= -->	
		


	<!-- Start of sponsor section
		============================================= -->
		<!-- <div id="sponsor" class="sponsor-section sponsor-2">
			<div class="container">
				<div class="sponsor-item">
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/picolino/1.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center ">
						<img src="{{asset('assets/img/sponsor/s-2.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-3.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-4.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-5.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-6.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-1.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-2.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-3.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-4.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-5.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-6.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-1.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-2.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-3.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-4.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-5.jpg')}}" alt="">
					</div>
					<div class="sponsor-pic text-center">
						<img src="{{asset('assets/img/sponsor/s-6.jpg')}}" alt="">
					</div>
				</div>
			</div>
		</div> -->
	<!-- End of sponsor section
		============================================= -->


	<!-- Start popular course
		============================================= -->
		<section id="popular-course" class="popular-course-section">
			<div class="container">
				<div class="section-title mb20 headline text-left">
					<span class="subtitle text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
					<h2>Estas son nuestras <span>Escuelas</span></h2>
				</div>
				<div id="course-slide-item" class="course-slide">
					<div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/piccolino/19.jpg')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>Info</span>
							</div>
							<div class="course-details-btn">
								<a href="#">Vinculación <i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<span class="course-category bold-font"><a href="#">..............................</a></span>
								<span class="course-author bold-font"><a href="#">LA SALLE</a></span>
								<!-- <div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div> -->
								<span class="course-category bold-font"><a href="#">............................</a></span>

							</div>
							<!-- <div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
							</div> -->
							<div class="course-viewer ul-li">
								<ul>
									<!-- <li><a href=""><i class="fas fa-user"></i>Horario:</a></li> -->
									<li><a href="">Horario: Domingos de 7 a.m. a 2 p.m.</a></li>

									<li><a href="">Ubicación: Cra. 5 No. 59A-44</a></li>
									<!-- <li><a href="">Cra. 5 No. 59A-44</a></li> -->
								</ul>
							</div>
						</div>
					</div>
					<!-- /item -->
					<!-- item -->

					<div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/piccolino/20.png')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>Info</span>
							</div>
							<div class="course-details-btn">
								<a href="#">Vinculación<i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<!-- <span class="course-category bold-font"><a href="#">Mobile Apps</a></span>
								<span class="course-author bold-font"><a href="#">Fernando Torres</a></span>
								<div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div> -->
								<span class="course-category bold-font"><a href="#">..............................</a></span>
								<span class="course-author bold-font"><a href="#">LISBOA</a></span>
								<span class="course-category bold-font"><a href="#">............................</a></span>
							</div>
							<!-- <div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Introduction to Mobile Application Development.</a></h3>
							</div> -->
							<div class="course-viewer ul-li">
									<!-- <li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
									<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
									<li><a href="">125k Unrolled</a></li> -->
									<ul>
									<!-- <li><a href=""><i class="fas fa-user"></i>Horario:</a></li> -->
									<li><a href="">Horario: Domingos de 8 a.m. a 1 p.m.</a></li>

									<li><a href="">Ubicación: Iglesia Beato Juan Bautista Scalabrini.</a></li>
									<!-- <li><a href="">Cra. 5 No. 59A-44</a></li> -->
								</ul>
							</div>
						</div>
					</div>
					<!-- /item -->

					<div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/piccolino/21.png')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>Info</span>
							</div>
							<div class="course-details-btn">
								<a href="#">Vinculación<i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<!-- <span class="course-category bold-font"><a href="#">Motion Graphic </a></span>
								<span class="course-author bold-font"><a href="#">enny Garcias</a></span>
								<div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div> -->
								<span class="course-category bold-font"><a href="#">.........................</a></span>
								<span class="course-author bold-font"><a href="#">SAN FRANCISCO</a></span>
								<span class="course-category bold-font"><a href="#">.....................</a></span>
							</div>
							<!-- <div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Learning IOS Apps Programming & Development.</a></h3>
							</div> -->
							<div class="course-viewer ul-li">
								<!-- <ul>
									<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
									<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
									<li><a href="">125k Unrolled</a></li>
								</ul> -->
								<ul>
									<!-- <li><a href=""><i class="fas fa-user"></i>Horario:</a></li> -->
									<li><a href="">Horario: Domingos de 8 a.m. a 1 p.m.</a></li>

									<li><a href="">Ubicación: Casa de la cultura de ciudad Bolívar.</a></li>
									<!-- <li><a href="">Cra. 5 No. 59A-44</a></li> -->
								</ul>
							</div>
						</div>
					</div>
					<!-- /item -->

					<div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/piccolino/22.png')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>Info</span>
							</div>
							<div class="course-details-btn">
								<a href="#">Vinculación<i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<!-- <span class="course-category bold-font"><a href="#">Web Design</a></span>
								<span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
								<div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div> -->
								<span class="course-category bold-font"><a href="#">.........................</a></span>
								<span class="course-author bold-font"><a href="#">RECUERDO BAJO</a></span>
								<span class="course-category bold-font"><a href="#">.....................</a></span>
							</div>
							<!-- <div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
							</div> -->
							<div class="course-viewer ul-li">
								<!-- <ul>
									<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
									<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
									<li><a href="">125k Unrolled</a></li>
								</ul> -->
								<ul>
									<li><a href="">Horario: Domingos de 8 a.m. a 1 p.m.</a></li>
									<li><a href="">Ubicación:Ciudad Bolívar.</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /item -->

					<!-- <div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/course/c-3.jpg')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>$99.00</span>
							</div>
							<div class="course-details-btn">
								<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<span class="course-category bold-font"><a href="#">Web Design</a></span>
								<span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
								<div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div>
							</div>
							<div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
							</div>
							<div class="course-viewer ul-li">
								<ul>
									<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
									<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
									<li><a href="">125k Unrolled</a></li>
								</ul>
							</div>
						</div>
					</div> -->
					<!-- /item -->

					<!-- <div class="course-item-pic-text">
						<div class="course-pic relative-position mb25">
							<img src="{{asset('assets/img/course/c-1.jpg')}}" alt="">
							<div class="course-price text-center gradient-bg">
								<span>$99.00</span>
							</div>
							<div class="course-details-btn">
								<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
							</div>
						</div>
						<div class="course-item-text">
							<div class="course-meta">
								<span class="course-category bold-font"><a href="#">Web Design</a></span>
								<span class="course-author bold-font"><a href="#">John Luis Fernandes</a></span>
								<div class="course-rate ul-li">
									<ul>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
										<li><i class="fas fa-star"></i></li>
									</ul>
								</div>
							</div>
							<div class="course-title mt10 headline pb45 relative-position">
								<h3><a href="#">Fully Responsive Web Design & Development.</a> <span class="trend-badge text-uppercase bold-font"><i class="fas fa-bolt"></i> TRENDING</span></h3>
							</div>
							<div class="course-viewer ul-li">
								<ul>
									<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
									<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
									<li><a href="">125k Unrolled</a></li>
								</ul>
							</div>
						</div>
					</div> -->
					<!-- /item -->
				</div>
			</div>
		</section>
	<!-- End popular course
		============================================= -->


	<!-- Start of about us section
		============================================= -->
		<!-- <section id="about-us" class="about-us-section home-secound">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<div class="about-resigter-form backgroud-style relative-position">
							<div class="register-content">
								<div class="register-fomr-title text-center">
									<h3 class="bold-font"><span>Get a</span> Free Registration.</h3>
									<p>More Than 122K Online Available Courses</p>
								</div>
								<div class="register-form-area">
									<form class="contact_form" action="#" method="POST" enctype="multipart/form-data">
										<div class="contact-info">
											<input class="name" name="name" type="text" placeholder="Your Name.">
										</div>
										<div class="contact-info">
											<input class="nbm" name="nbm" type="number" placeholder="Your Number">
										</div>
										<div class="contact-info">
											<input class="email" name="email" type="email" placeholder="Email Address.">
										</div>
										<select>
											<option value="9" selected="">Select Course.</option>
											<option value="10">Web Design</option>
											<option value="11">Web Development</option>
											<option value="12">Php Core</option>
										</select>
										<div class="contact-info">
											<input type="text" id="datepicker" placeholder="Date.">
										</div>
										<textarea placeholder="Message."></textarea>
										<div class="nws-button text-uppercase text-center white text-capitalize">
											<button type="submit" value="Submit">SUBMIT REQUEST </button> 
										</div> 
									</form>
								</div>
							</div>
						</div>
						<div class="bg-mockup">
							<img src="{{asset('assets/img/about/abt.jpg')}}" alt="">
						</div>
					</div>
					<div class="col-md-7">
						<div class="about-us-text">
							<div class="section-title relative-position mb20 headline text-left">
								<span class="subtitle ml42 text-uppercase">SORT ABOUT US</span>
								<h2>We are <span>Genius Course</span>
								work since 1980.</h2>
								<p>We take our mission of increasing global access to quality education seriously. We connect learners to the best universities and institutions from around the world.</p>
							</div>
							<div class="about-content-text">
								<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam. magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
								<div class="about-list mb65 ul-li-block">
									<ul>
										<li>Professional And Experienced Since 1980</li>
										<li>We Connect Learners To The Best  Universities From Around The World</li>
										<li>Our Mission Increasing Global Access To Quality Aducation</li>
										<li>100K Online Available Courses</li>
									</ul>
								</div>
								<div class="about-btn">
									<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
										<a href="#">About Us <i class="fas fa-caret-right"></i></a>
									</div>
									<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
										<a href="#">contact us <i class="fas fa-caret-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End of about us section
		============================================= -->


	<!-- Start of Search Courses
		============================================= -->
		<section id="search-course" class="search-course-section home-secound-course-search backgroud-style">
			<div class="container">
				<div class="section-title mb20 headline text-center">
					<span class="subtitle text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
					<h2><span>VOLUNTARIADO</span> </h2>
				</div>
				<!-- <div class="search-course mb30 relative-position">
					<form action="#" method="post">
						<input class="course" name="course" type="text" placeholder="Type what do you want to learn today?">
						<div class="nws-button text-center  gradient-bg text-capitalize">
							<button type="submit" value="Submit">Search Course</button> 
						</div>
					</form>
				</div> -->
				<div class="search-counter-up">
					<div class="row">
						<div class="col-md-3">
							<div class="counter-icon-number">
								<div class="counter-icon">
									<i class="text-gradiant flaticon-graduation-hat"></i>
								</div>
								<div class="counter-number">
									<span class=" bold-font">Maestro</span>
									 <p>Voluntariado</p> 
								</div>
							</div>
						</div>
						<!-- /counter -->

						<div class="col-md-3">
							<div class="counter-icon-number">
								<div class="counter-icon">
									<i class="text-gradiant flaticon-book"></i>
								</div>
								<div class="counter-number">
									<span class="bold-font">Apoyo a niños</span>
									<p>Voluntariado</p> 
								</div>
							</div>
						</div>
						<!-- /counter -->

						<div class="col-md-3">
							<div class="counter-icon-number">
								<div class="counter-icon">
									<i class="text-gradiant flaticon-favorites-button"></i>
								</div>
								<div class="counter-number">
									<span class=" bold-font">Profesional</span>
									<p>Voluntariado</p> 
								</div>
							</div>
						</div>
						<!-- /counter -->

						<div class="col-md-3">
							<div class="counter-icon-number">
								<div class="counter-icon">
									<i class="text-gradiant flaticon-group"></i>
								</div>
								<div class="counter-number">
									<span class=" bold-font">Amigo</span>
									<p>Voluntariado</p> 
								</div>
							</div>
						</div>
						<!-- /counter -->
					</div>
				</div>

				<div class="search-app">
					<div class="row">
						<div class="col-md-6">
							<div class="app-mock-up">
								<img src="{{asset('assets/img/banner/banner Piccolino 5.jpg')}}" alt="">
							</div>
						</div>

						<div class="col-md-6">
							<div class="about-us-text search-app-content">
								<div class="section-title relative-position mb20 headline text-left">
									<h2 ><span  style="color:#4cb7f5;">Nos da mucho gusto que quieras unirte a nosotros como voluntario(a). Hemos establecido varias categorías para que puedas elegir.</span> </h2>
								</div>

								<div class="app-details-content">
									<!-- <p>Introduction Genius Mobile Application on Play Store lorem ipsum dolor sit amet consectuerer adipiscing.</p> -->

									<div class="about-list mb30 ul-li-block">
										<ul>
											<li>Maestro</li>
											<li>Apoyo para niños</li>
											<li>Profesional con saber específico</li>
											<li>Amigo</li>

										</ul>
									</div>
									<!-- <div class="about-btn">
										<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font float-left">
											<a href="#">GET THE APP NOW </a>
										</div>

										<div class="app-stor ul-li mt10">
											<ul>
												<li><a href="#"><i class="fab fa-android"></i></a></li>
												<li><a href="#"><i class="fab fa-apple"></i></a></li>
												<li><a href="#"><i class="fab fa-windows"></i></a></li>
											</ul>
										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- End of Search Courses
		============================================= -->


	<!-- Start latest section
		============================================= -->
		<!-- <section id="latest-area" class="latest-area-section">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="latest-area-content">
							<div class="section-title-2 mb65 headline text-left">
								<h2>Latest <span>News.</span></h2>
							</div>
							<div class="latest-news-posts">
								<div class="latest-news-area">
									<div class="latest-news-thumbnile relative-position">
										<img src="assets/img/blog/lb-1.jpg" alt="">
										<div class="hover-search">
											<i class="fas fa-search"></i>
										</div>
										<div class="blakish-overlay"></div>
									</div>
									<div class="date-meta">
										<i class="fas fa-calendar-alt"></i> 26 April 2018
									</div>
									<h3 class="latest-title bold-font"><a href="#">Affiliate Marketing A Beginner’s Guide.</a></h3>
									<div class="course-viewer ul-li">
										<ul>
											<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
											<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
										</ul>
									</div>
								</div>

								<div class="latest-news-posts">
									<div class="latest-news-area">
										<div class="latest-news-thumbnile relative-position">
											<img src="{{asset('assets/img/blog/lb-2.jpg')}}" alt="">
											<div class="hover-search">
												<i class="fas fa-search"></i>
											</div>
											<div class="blakish-overlay"></div>
										</div>
										<div class="date-meta">
											<i class="fas fa-calendar-alt"></i> 26 April 2018
										</div>
										<h3 class="latest-title bold-font"><a href="#">No.1 The Best Online Course 2018.</a></h3>
										<div class="course-viewer ul-li">
											<ul>
												<li><a href=""><i class="fas fa-user"></i> 1.220</a></li>
												<li><a href=""><i class="fas fa-comment-dots"></i> 1.015</a></li>
											</ul>
										</div>
									</div>
								</div>

								<div class="view-all-btn bold-font">
									<a href="#">View All News <i class="fas fa-chevron-circle-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="latest-area-content">
							<div class="section-title-2 mb65 headline text-left">
								<h2>Upcoming <span>Events.</span></h2>
							</div>
							<div class="latest-events">
								<div class="latest-event-item">
									<div class="events-date  relative-position text-center">
										<div class="gradient-bdr"></div>
										<span class="event-date bold-font">22</span>
										April 2018
									</div>
									<div class="event-text">
										<h3 class="latest-title bold-font"><a href="#">Fully Responsive Web Design & Development.</a></h3>
										<div class="course-meta">
											<span class="course-category"><a href="#">Web Design</a></span>
											<span class="course-author"><a href="#">Koke</a></span>
										</div>
									</div>
								</div>
							</div>

							<div class="latest-events">
								<div class="latest-event-item">
									<div class="events-date  relative-position text-center">
										<div class="gradient-bdr"></div>
										<span class="event-date bold-font">07</span>
										August 2018
									</div>
									<div class="event-text">
										<h3 class="latest-title bold-font"><a href="#">Introduction to Mobile Application Development.</a></h3>
										<div class="course-meta">
											<span class="course-category"><a href="#">Web Design</a></span>
											<span class="course-author"><a href="#">Koke</a></span>
										</div>
									</div>
								</div>
							</div>

							<div class="latest-events">
								<div class="latest-event-item">
									<div class="events-date  relative-position text-center">
										<div class="gradient-bdr"></div>
										<span class="event-date bold-font">30</span>
										Sept 2018
									</div>
									<div class="event-text">
										<h3 class="latest-title bold-font"><a href="#">IOS Apps Programming & Development.</a></h3>
										<div class="course-meta">
											<span class="course-category"><a href="#">Web Design</a></span>
											<span class="course-author"><a href="#">Koke</a></span>
										</div>
									</div>
								</div>
							</div>

							<div class="view-all-btn bold-font">
								<a  href="#">Check Calendar   <i class="fas fa-calendar-alt"></i></a>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="latest-area-content">
							<div class="section-title-2 mb65 headline text-left">
								<h2>Latest <span>Video.</span></h2>
							</div>
							<div class="latest-video-poster relative-position mb20">
								<img src="assets/img/banner/v-1.jpg" alt="">
								<div class="video-play-btn text-center gradient-bg">
									<a class="popup-with-zoom-anim" href="https://www.youtube.com/watch?v=-g4TnixUdSc"><i class="fas fa-play"></i></a>
								</div>
							</div>
							<h3 class="latest-title bold-font"><a href="#">Learning IOS Apps in Amsterdam.</a></h3>
							<p class="mb25">Lorem ipsum dolor sit amet, consectetuer delacosta adipiscing elit, sed diam nonummy.</p>
							<div class="view-all-btn bold-font">
								<a href="#">View All Videos <i class="fas fa-chevron-circle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End latest section
		============================================= -->


	<!-- Start of best product section
		============================================= -->
		<!-- <section id="best-product" class="best-product-section home_2">
			<div class="container">
				<div class="section-title-2 mb65 headline text-left">
					<h2>Genius <span>Best Products.</span></h2>
				</div>
				<div id="best-product-slide-item"  class="best-product-slide">
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-1.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>

					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-2.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-3.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-4.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-1.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-2.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
					<div class="product-img-text">
						<div class="product-img text-center mb20">
							<img src="{{asset('assets/img/product/bp-3.png')}}" alt="">
						</div>
						<div class="product-text-content relative-position">
							<div class="best-title-price float-left">
								<div class="course-title headline">
									<h3><a href="#">Mobile Apps Books.</a></h3>
								</div>
								<div class="price-start">
									Start from
									<span>$55.25</span>
								</div>
							</div>
							<div class="add-cart text-center">
								<i class="fas fa-cart-plus"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End  of best product section
		============================================= -->


	<!-- Start of best course
		============================================= -->
		<!-- <section id="best-course" class="best-course-section">
			<div class="container">
				<div class="section-title mb45 headline text-center">
					<span class="subtitle text-uppercase">SEARCH OUR COURSES</span>
					<h2>Browse Our<span> Best Course.</span></h2>
				</div>
				<div class="best-course-area mb45">
					<div class="row">
						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-1.jpg')}}" alt="">
									<div class="trend-badge-2 text-center text-uppercase">
										<i class="fas fa-bolt"></i>
										<span>Trending</span>
									</div>
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-2.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-3.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-4.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-5.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-6.jpg')}}" alt="">
									<div class="trend-badge-2 text-center text-uppercase">
										<i class="fas fa-bolt"></i>
										<span>Trending</span>
									</div>
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-7.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3">
							<div class="best-course-pic-text relative-position">
								<div class="best-course-pic relative-position">
									<img src="{{asset('assets/img/course/bc-8.jpg')}}" alt="">
									<div class="course-price text-center gradient-bg">
										<span>$99.00</span>
									</div>
									<div class="course-rate ul-li">
										<ul>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
											<li><i class="fas fa-star"></i></li>
										</ul>
									</div>
									<div class="course-details-btn">
										<a href="#">COURSE DETAIL <i class="fas fa-arrow-right"></i></a>
									</div>
									<div class="blakish-overlay"></div>
								</div>
								<div class="best-course-text">
									<div class="course-title mb20 headline relative-position">
										<h3><a href="#">Fully Responsive Web Design &amp; Development.</a></h3>
									</div>
									<div class="course-meta">
										<span class="course-category"><a href="#">Web Design</a></span>
										<span class="course-author"><a href="#">250 Students</a></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				
			</div>
		</section> -->
	<!-- End of best course
		============================================= -->


	<!-- Start FAQ section
		============================================= -->
		<!-- <section id="faq" class="faq-section faq-secound-home-version backgroud-style">
			<div class="container">
				<div class="section-title mb45 headline text-center">
					<span class="subtitle text-uppercase">GENIUS COURSE FAQ</span>
					<h2>Frequently<span> Ask & Questions</span></h2>
				</div>

				<div class="faq-tab mb45">
					<div class="faq-tab-ques  ul-li">
						<div class="tab-button text-center mb45">
							<ul class="product-tab">
								<li class="active" rel="tab1">GENERAL </li>
								<li rel="tab2"> COURSES </li>
								<li rel="tab3"> TEACHERS </li>
								<li rel="tab4">  EVENTS  </li>
								<li rel="tab5">  OTHERS  </li>
							</ul>
						</div>

						<div class="tab-container">

							<div id="tab1" class="tab-content-1 pt35">
								<div id="accordion" class="panel-group">
									<div class="panel">
										<div class="panel-title" id="headingOne">
											<h3 class="mb-0">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													How to Register or Make An Account in Genius?
												</button>
											</h3>
										</div>
										<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingTwo">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
													What is Genius Courses?
												</button>
											</h3>
										</div>
										<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingThree">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
													What Lorem Ipsum Dolor Sit Amet Consectuerer?
												</button>
											</h3>
										</div>
										<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingFoure">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFoure" aria-expanded="false" aria-controls="collapseFoure">
													Adipiscing Diamet Nonnumy Nibh Euismod?
												</button>
											</h3>
										</div>
										<div id="collapseFoure" class="collapse"  data-parent="#accordion">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
								</div>

							</div>

							<div id="tab2" class="tab-content-1 pt35">
								<div id="accordion-2" class="panel-group">
									<div class="panel">
										<div class="panel-title" id="headingSix">
											<h3 class="mb-0">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
													How to Register or Make An Account in Genius?
												</button>
											</h3>
										</div>
										<div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordion-2">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingSeven">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
													What is Genius Courses?
												</button>
											</h3>
										</div>
										<div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion-2">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingEight">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
													What Lorem Ipsum Dolor Sit Amet Consectuerer?
												</button>
											</h3>
										</div>
										<div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion-2">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
									<div class="panel">
										<div class="panel-title" id="headingNine">
											<h3 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
													Adipiscing Diamet Nonnumy Nibh Euismod?
												</button>
											</h3>
										</div>
										<div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion-2">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam consectetuer adipiscing elit, sed diam nonummy.
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="tab3" class="tab-content-1 pt35">
								<div class="row">
									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> What is Genius Courses?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> What Lorem Ipsum Dolor Sit Amet Consectuerer?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>

									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> How to Register or Make An Account in Genius?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> Adipiscing Diamet Nonnumy Nibh Euismod?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>
								</div>
							</div>

							<div id="tab4" class="tab-content-1 pt35">
								<div class="row">
									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> What is Genius Courses?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> What Lorem Ipsum Dolor Sit Amet Consectuerer?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>

									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> How to Register or Make An Account in Genius?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> Adipiscing Diamet Nonnumy Nibh Euismod?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>
								</div>
							</div>

							<div id="tab5" class="tab-content-1 pt35">
								<div class="row">
									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> What is Genius Courses?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> What Lorem Ipsum Dolor Sit Amet Consectuerer?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>

									<div class="col-md-6">
										<div class="ques-ans mb45 headline">
											<h3> How to Register or Make An Account in Genius?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>

										<div class="ques-ans mb45 headline">
											<h3> Adipiscing Diamet Nonnumy Nibh Euismod?</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam volutpat. Ut wisi enim ad minim veniam.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="about-btn text-center">
					<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
						<a href="#">Make Question <i class="fas fa-caret-right"></i></a>
					</div>
					<div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
						<a href="#">contact us <i class="fas fa-caret-right"></i></a>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End FAQ section
		============================================= -->


	<!-- Start Course category
		============================================= -->
		<section id="course-category" class="course-category-section home-secound-version">
			<div class="container">
				<div class="section-title mb20 headline text-left">
					<span class="subtitle ml42  text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
					<h2>Nuestros <span>valores:</span></h2>
				</div>
				<div class="category-item category-slide-item">
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology"></i>
							</div>
							<div class="category-title">
								<h4>Defensa de los derechos y de la dignidad humana, en especial de la infancia.</h4>
							</div>
						</div>
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-app-store"></i>
							</div>
							<div class="category-title">
								<h4>Responsabilidad, integridad y coherencia.</h4>
							</div>
						</div>
					</div>
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-artist-tools"></i>
							</div>
							<div class="category-title">
								<h4>Compromiso desde la ética, la participación y la solidaridad.</h4>
							</div>
						</div>
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-business"></i>
							</div>
							<div class="category-title">
								<h4>Promoción y respeto de la diversidad cultural y la equidad de género.</h4>
							</div>
						</div>
					</div>

					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-dna"></i>
							</div>
							<div class="category-title">
								<h4>Ecología y sostenibilidad ambiental.</h4>
							</div>
						</div>

						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-cogwheel"></i>
							</div>
							<div class="category-title">
								<h4>Cercanía a la gente y orientación a sus problemas concretos.</h4>
							</div>
						</div>
					</div>
					
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology-1"></i>
							</div>
							<div class="category-title">
								<h4>Apuesta por el desarrollo de las capacidades humanas.</h4>
							</div>
						</div>

						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology-2"></i>
							</div>
							<div class="category-title">
								<h4>Compromiso con las personas y colectivos con mayor vulnerabilidad social.</h4>
							</div>
						</div>
					</div>
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology"></i>
							</div>
							<div class="category-title">
								<h4>Transparencia y buena gestión de nuestros recursos y actividades.</h4>
							</div>
						</div>
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-app-store"></i>
							</div>
							<div class="category-title">
								<h4>Cooperación y coordinación con otros agentes públicos y privados.</h4>
							</div>
						</div>
					</div>
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-artist-tools"></i>
							</div>
							<div class="category-title">
								<h4>Ecología y sostenibilidad ambiental.</h4>
							</div>
						</div>
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-business"></i>
							</div>
							<div class="category-title">
								<h4>Cercanía a la gente y orientación a sus problemas concretos.</h4>
							</div>
						</div>
					</div>

					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-dna"></i>
							</div>
							<div class="category-title">
								<h4>Apuesta por el desarrollo de las capacidades humanas.</h4>
							</div>
						</div>

						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-cogwheel"></i>
							</div>
							<div class="category-title">
								<h4>Compromiso con las personas y colectivos con mayor vulnerabilidad social.</h4>
							</div>
						</div>
					</div>
					
					<div class="category-slide-content">
						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology-1"></i>
							</div>
							<div class="category-title">
								<h4>Transparencia y buena gestión de nuestros recursos y actividades.</h4>
							</div>
						</div>

						<div class="category-icon-title text-center">
							<div class="category-icon">
								<i class="text-gradiant flaticon-technology-2"></i>
							</div>
							<div class="category-title">
								<h4>Cooperación y coordinación con otros agentes públicos y privados.</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	<!-- End Course category
		============================================= -->


	<!-- Start secound testimonial section
		============================================= -->
		<!-- <section id="testimonial-secound" class="secound-testimoinial-section">
			<div class="container">
				<div class="testimonial-slide">
					<div class="section-title mb35 headline text-center">
						<span class="subtitle text-uppercase">WHAT THEY SAY ABOUT US</span>
						<h2>Students <span>Testimonial.</span></h2>
					</div>

					<div class="testimonial-secound-slide-area">
						<div class="student-qoute text-center">
							<p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience so we’ll be back in the future lorem ipsum diamet.”</p>
							<div class="student-name-designation">
								<span class="st-name bold-font">Robertho Garcia </span>
								<span class="st-designation">Graphic Designer</span>
							</div>
						</div>

						<div class="student-qoute text-center">
							<p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience so we’ll be back in the future lorem ipsum diamet.”</p>
							<div class="student-name-designation">
								<span class="st-name bold-font">Robertho Garcia </span>
								<span class="st-designation">Graphic Designer</span>
							</div>
						</div>

						<div class="student-qoute text-center">
							<p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience so we’ll be back in the future lorem ipsum diamet.”</p>
							<div class="student-name-designation">
								<span class="st-name bold-font">Robertho Garcia </span>
								<span class="st-designation">Graphic Designer</span>
							</div>
						</div>

						<div class="student-qoute text-center">
							<p>“This was our first time lorem ipsum and we <b> were very pleased with the whole experience</b>. Your price was lower than other companies. Our experience so we’ll be back in the future lorem ipsum diamet.”</p>
							<div class="student-name-designation">
								<span class="st-name bold-font">Robertho Garcia </span>
								<span class="st-designation">Graphic Designer</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	<!-- End secound testimonial section
		============================================= -->


	<!-- Start secound teacher section
		============================================= -->
		<section id="teacher-2" class="secound-teacher-section">
			<div class="container">
				<div class="section-title mb35 headline text-left">
					<span class="subtitle ml42  text-uppercase">HOLA! COMUNIDAD PICCOLINO</span>
					<h2><span>Voluntarios</span> Activos</h2>
				</div>
				<div class="teacher-secound-slide">

				@foreach ($peoples as $item)
				<div class="teacher-img-text relative-position text-center">
						<div class="teacher-img-social relative-position">
						<img src="{{asset('storage/people/images/'.$item->imagen)}}" alt="">
							<div class="blakish-overlay"></div>
							<!-- <div class="teacher-social-list ul-li">
								<ul>
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
								</ul>
							</div> -->
						</div>
						<div class="teacher-name-designation mt15">
						<span class="teacher-name">{{$item->names}} {{$item->surnames}}</span>
							<span class="teacher-designation">{{$item->typePeople->name}}</span>
						</div>
					</div>

				<!-- <div class="teacher-secound-slide">

                <div class="teacher-img-text relative-position text-center">
						<div class="teacher-img-social relative-position">
							<img src="{{asset('storage/people/images/'.$item->imagen)}}" alt="">
							 <div class="blakish-overlay"></div> 
							 <div class="teacher-social-list ul-li">
								
							</div>
						</div>
						<div class="teacher-name-designation mt15">
							<span class="teacher-name">{{$item->names}} {{$item->surnames}}</span>
							<span class="teacher-designation">{{$item->typePeople->name}}</span>
						</div>
					</div>  
                </div>                  -->
                @endforeach


				
				</div>

				<!-- <div class="genius-btn gradient-bg text-center text-uppercase ul-li-block bold-font">
					<a href="#">All teacher <i class="fas fa-caret-right"></i></a>
				</div> -->
			</div>
		</section>
	<!-- End teacher section
		============================================= -->



	<!-- Start Of scound contact section
		============================================= -->
		@include('web.footer.footer')

	<!-- ENd Of scound contact section
		============================================= -->





		<!-- For Js Library -->
		<script src="{{asset('assets/js/jquery-2.1.4.min.js')}}"></script>
		<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/js/popper.min.js')}}"></script>
		<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/js/jarallax.js')}}"></script>
		<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/js/lightbox.js')}}"></script>
		<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
		<script src="{{asset('assets/js/scrollreveal.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
		<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
		<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
		<script src="{{asset('assets/js/gmap3.min.js')}}"></script>
		<script src="{{asset('assets/js/switch.js')}}"></script>
		<script src="http://maps.google.com/maps/api/js?key=AIzaSyC61_QVqt9LAhwFdlQmsNwi5aUJy9B2SyA"></script>

		<script src="{{asset('assets/js/script.js')}}"></script>
	</body>
	</html>

<!-- <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html> -->

<aside class="left-sidebar">
    <div class="d-flex no-block nav-text-box align-items-center">
        <span><img src="{{asset('template/images/logo-icon.png')}}" alt="elegant admin template"></span>
        <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i class="mdi mdi-toggle-switch"></i></a>
        <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
    </div>
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-item">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu1"><i class="icon-lock"></i> <span class="hide-menu">Seguridad</span></a>
                    <div class="collapse" id="submenu1" aria-expanded="false">
                        <ul class="flex-column pl-2 nav">
                            <li class="nav-item">
                                <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" data-toggle="collapse" data-target="#submenu1sub1"><i class="icon-people"></i><span class="hide-menu">Usuarios</span></a>
                                <div class="collapse" id="submenu1sub1" aria-expanded="false">
                                    <ul class="flex-column nav pl-4">
                                        
                                </div>
                        </ul>
                    </div>
                </li>
           
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
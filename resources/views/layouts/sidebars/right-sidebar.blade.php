<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Panel de servicio. <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul id="themecolors" class="m-t-20">
                <li><b>Con barra lateral de luz</b></li>
                <li><a href="javascript:void(0)" data-skin="skin-default" class="default-theme" onclick="changeSkin('skin-default','{{((auth()->user()) ? auth()->user()->id: null )}}')">1</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-green" class="green-theme" onclick="changeSkin('skin-skin-green','{{((auth()->user()) ? auth()->user()->id: null )}}')">2</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-red" class="red-theme" onclick="changeSkin('skin-red','{{((auth()->user()) ? auth()->user()->id: null )}}')">3</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-blue" class="blue-theme" onclick="changeSkin('skin-blue','{{((auth()->user()) ? auth()->user()->id: null )}}')">4</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-purple" class="purple-theme" onclick="changeSkin('skin-purple','{{((auth()->user()) ? auth()->user()->id: null )}}')">5</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-megna" class="megna-theme" onclick="changeSkin('skin-megna','{{((auth()->user()) ? auth()->user()->id: null )}}')">6</a></li>
                <li class="d-block m-t-30"><b>Con barra lateral Oscuro</b></li>
                <li><a href="javascript:void(0)" data-skin="skin-default-dark" class="default-dark-theme working" onclick="changeSkin('skin-default-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">7</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-green-dark" class="green-dark-theme" onclick="changeSkin('skin-green-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">8</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-red-dark" class="red-dark-theme" onclick="changeSkin('skin-red-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">9</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-blue-dark" class="blue-dark-theme" onclick="changeSkin('skin-blue-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">10</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-purple-dark" class="purple-dark-theme" onclick="changeSkin('skin-purple-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">11</a></li>
                <li><a href="javascript:void(0)" data-skin="skin-megna-dark" class="megna-dark-theme " onclick="changeSkin('skin-megna-dark','{{((auth()->user()) ? auth()->user()->id: null )}}')">12</a></li>
            </ul>
            <!-- <ul class="m-t-20 chatonline">
                <li><b>Chat option</b></li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/1.jpg')}}" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/2.jpg')}}" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/3.jpg')}}" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/4.jpg')}}" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/5.jpg')}}" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/6.jpg')}}" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/7.jpg')}}" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="{{asset('template/images/users/8.jpg')}}" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                </li>
            </ul> -->
        </div>
    </div>
</div>

<script>
    
        function changeSkin(newSkin,id) {
            $.get('changeSkin/' + id, {
                skin: newSkin
            }, function(data) {

            });
        }
  
</script>
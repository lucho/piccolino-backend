<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeadquarterCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headquarter_costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('value');
            $table->unsignedBigInteger('headquarter_id');
            $table->text('observations')->nullable();
            $table->text('purchase_concept')->nullable();
            $table->text('resources_generated_by')->nullable();
            $table->unsignedBigInteger('semester_id')->nullable();
            $table->text('invoice_url')->nullable();
            $table->date('date')->nullable();
            $table->boolean('enabled')->default(true);
            $table->unsignedBigInteger('user_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('headquarter_costs');
    }
}

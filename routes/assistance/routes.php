<?php
//Asistencia
Route::apiResource('assistances', 'Schools\AssistanceController', ['only' => [
    'index',
    'show',
    'store',
    'update',
    'destroy',
]]);
Route::get('assistance/datatable', 'Schools\AssistanceController@dataTable');
Route::get('assistance/dependences', 'Schools\AssistanceController@dependences');

<?php

use Illuminate\Support\Facades\Route;
use App\Models\People\{People,Document,Contact,AcademicInformation,History};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include_once __DIR__ . '/skin/routes.php';

Route::get('/', function () {
    $people = People::with('typeDocument','typePeople')->whereIn('type_people_id', [3, 4, 5, 6, 7, 8, 9]);
    $people->where('enabled',1);
    $peoples = $people->get();
    return view('welcome')->with(compact('peoples'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/**
 * Views
 */
Route::get('/sightWho', 'web\ViewController@viewQuienesSomos')->name('sightWho');
Route::get('/history', 'web\ViewController@viewHistoria')->name('history');
Route::get('/ourFounder', 'web\ViewController@viewNuestroFundador')->name('ourFounder');
Route::get('/theSalle', 'web\ViewController@viewLaSalle')->name('theSalle');
Route::get('/lisboa', 'web\ViewController@viewLisboa')->name('lisboa');
Route::get('/sanFrancisco', 'web\ViewController@viewSanFrancisco')->name('sanFrancisco');
Route::get('/weMake', 'web\ViewController@viewWeMake')->name('weMake');
Route::get('/volunteering', 'web\ViewController@viewVolunteering')->name('volunteering');
Route::get('/resources', 'web\ViewController@viewResources')->name('resources');
Route::get('/conventions', 'web\ViewController@viewConventions')->name('conventions');
Route::get('/lounsSymboloe', 'web\ViewController@lounsSymboloe')->name('lounsSymboloe');











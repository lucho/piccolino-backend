<?php






// Dependencias
Route::get('groupsDependencesExport', 'Schools\GroupController@dependencesExport');
Route::get('incomeCostsAndExpensesDependences', 'Schools\HeadquarterCostController@dependences');

/**
 * Reporte de gastos Sede
 */
Route::get('schoolAccountingReportBySemester', 'Schools\HeadquarterController@schoolAccountingReportBySemester');

/**
 * Reporte del número de matrículas
 */
Route::get('toObtainTheNumberOfEnrolmentsPerSemester', 'Schools\EnrolledController@toObtainTheNumberOfEnrolmentsPerSemester');






